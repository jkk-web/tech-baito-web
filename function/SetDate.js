const setToday = () => {
    var today = new Date();
    today.setDate(today.getDate());
    var yyyy = today.getFullYear();
    var mm = ("0"+(today.getMonth()+1)).slice(-2);
    var dd = ("0"+today.getDate()).slice(-2);
    document.getElementById("start_post").value=yyyy+'-'+mm+'-'+dd;
}

const setOneMonthLater = () => {
    var today = new Date();
    today.setDate(today.getDate());
    var yyyy = today.getFullYear();
    var mm = ("0"+(today.getMonth()+2)).slice(-2);
    var dd = ("0"+today.getDate()).slice(-2);
    document.getElementById("finish_post").value=yyyy+'-'+mm+'-'+dd;
}

export {setToday,setOneMonthLater}