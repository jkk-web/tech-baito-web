const AnalyzeSearchResult = (data, skills, tags) =>{
    let resultData = [];
    console.log(skills);
    console.log(tags);
    let languages = DivideData(skills,0);
    let frameworks = DivideData(skills,1);
    let minlevels = DivideData(skills,2);
    let maxlevels = DivideData(skills,3);
    let arrayTags = ToDataArray(tags);
    console.log(languages)
    console.log(arrayTags)
    console.log(arrayTags[0])

    if(languages[0]==="" && arrayTags[0]===""){
        console.log("タグとスキルの指定がないのでそのまま返します")
        return data;
    }

    for(let i = 0; i < data.length; i++){ //求人の数だけfor文
        let recLang = data[i].language.split(',');
        let recFrame = data[i].framework.split(',');
        let recLevel = data[i].level.split(',');
        let recTag = data[i].content.split(',');

        if(languages[0] != "")
            if(checkSkill(resultData,data[i],languages,frameworks,minlevels,maxlevels,recLang,recFrame,recLevel))
                continue;
        if(tags[0] != "")
            if(checkTag(resultData,data[i],arrayTags,recTag))
                continue;

    }
    return resultData;
}

const checkSkill = (result,data,languages,frameworks,minlevels,maxlevels,recLang,recFrame,recLevel) =>{
    for(let j = 0; j < recLang.length; j++){ //求人に掲載されたスキルの数だけ
        for(let k = 0; k < languages.length; k++){ //ユーザの検索したスキルの数だけ
            if(recLang[j] === languages[k]){ //言語が一致したら
                if(recFrame[j] === "anything" || recFrame[j] === frameworks[k]){ //求人にフレームワークが設定されていないor フレームワークが一致したら
                    if(minlevels[k] <=  recLevel[j] && recLevel[j] <= maxlevels[k]){ //レベルがユーザの要求を満たしていれば
                        result.push(data); //ユーザの条件にヒットするので正式な検索結果に追加
                        return true;
                    }
                }
            }
        }
    }
    return false;
}

const checkTag = (result,data,requiredTag,recTag) =>{
    for(let i = 0; i < recTag.length; i++){ //求人に掲載されたタグの数だけ
        for(let j = 0; j < requiredTag.length; j++){ //ユーザの検索したタグの数だけ
            if(recTag[i] === requiredTag[j]){ //タグが一致したら
                result.push(data); //ユーザの条件にヒットするので正式な検索結果に追加
                return true;
            }
        }
    }
    return false;
}

const DivideData = (skillData, i) =>{
    let data=[];
    for(let j = 0; j < skillData.length; j++){
        data.push(skillData[j][i]);
    }
    return data;
}

const ToDataArray = (tagData) => {
    let data=[];
    for(let j = 0; j < tagData.length; j++){
        data.push(tagData[j][0]);
    }
    return data;
}
/*
const NumToString = (data) =>{
    for(let i = 0; i < data.length; i++){ //ユーザの求める求人の数だけfor文
        if(recFrame[j] === "anything" || recFrame[j] === frameworks[k]){ //求人にフレームワークが設定されていないor フレームワークが一致したら
                        if(minlevels[k] <=  recLevel[j] && recLevel[j] <= maxlevels[k]){ //レベルがユーザの要求を満たしていれば
                            result.push(data); //ユーザの条件にヒットするので正式な検索結果に追加
                            return true;
                        }
                    }
                }
            }
        }
    }
}
*/
export { AnalyzeSearchResult };
