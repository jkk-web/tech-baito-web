const get_prefecture_api = "https://opendata.resas-portal.go.jp/api/v1/prefectures";
const get_city_api = "https://opendata.resas-portal.go.jp/api/v1/cities";

let PrefDataSet;
const maxSkillsNum = 3;
const maxTagsNum = 5;
let RemovedFrameworks;

const ReadCSV = (file, callback) => {
  //CSVファイルを文字列で取得。
  var txt = new XMLHttpRequest();
  //txt.responseType = 'text';
  txt.open('get',file , true);
  txt.onload = function (e) {
    if (txt.readyState === 4) {
      if (txt.status === 200) {
        var arr = txt.responseText.split('\n');

        //1次元配列を2次元配列に変換
        var res = [];
        for(var i = 0; i < arr.length; i++){
          //空白行が出てきた時点で終了
          if(arr[i] == '') break;

          //","ごとに配列化
          res[i] = arr[i].split(',');
          /*for(var j = 0; j < res[i].length; j++){
            //数字の場合は「"」を削除
            if(res[i][j].match(/\-?\d+(.\d+)?(e[\+\-]d+)?/)){
              res[i][j] = parseFloat(res[i][j].replace('"', ''));
            }
          }*/
        }
        callback(res);
      } else {
        console.error(txt.statusText);
      }
    }
  };
  txt.send();
}

const SetUpStation = (prefName) =>{
  ReadCSV("/data/station20200316free.csv",function(stations,err){
    let prefCode = SearchPrefCode(prefName);
    let station = document.forms.Form.SelectStation;
    let j = 0;
    removeChildren(station);
    station.options[j++] = new Option("選択されていません");
    for(let i=0;i<stations.length;i++){
      if(stations[i][3] === prefCode.toString()){
        station.options[j] = new Option(stations[i][1],stations[i][0]);
        j++;
      }
    }
  });
}


//languages[i][1]に言語名(表示用)．languages[i][0]に対応する数字が入ってます．返す値など適宜編集してください
const SetUpLanguage = (number) => {
  ReadCSV("/data/language.csv",function(languages,err){
    let formName = 'skills' + number.toString() +'language';
    let language = document.getElementById(formName);
    let j = 0;
    removeChildren(language);
    language.options[j++] = new Option("指定しない",0);
    for(let i=1;i<languages.length;i++){
      language.options[j] = new Option(languages[i][1],languages[i][0]);
      j++;
    }
  });
}

//languages[i][0]に言語id．languages[i][1]にframeworkID,languages[i][2]にフレームワーク名が入ってます．返す値など適宜編集してください
const SetUpFramework = (langID,number) => {
  ReadCSV("/data/framework.csv",function(frameworks,err){
    let formName = 'skills' + number.toString() +'framework';
    let framework = document.getElementById(formName);
    console.log(framework);
    let j=0;
    removeChildren(framework);
    framework.options[j++] = new Option("指定しない",0);
    for(let i=1;i<frameworks.length;i++){
      if(frameworks[i][0] === langID){
        if(!checkRemoved(frameworks[i][0],frameworks[i][1])){
          framework.options[j] = new Option(frameworks[i][2],frameworks[i][1]);
          j++;
        }
      }
    }
  });
}

const checkRemoved = (langID,frameID) =>{
  if(typeof RemovedFrameworks !== 'undefined')
    for(let i=0; i<RemovedFrameworks.length; i++)
      if(langID === RemovedFrameworks[i][0] && frameID === RemovedFrameworks[i][1])
        return true;
  return false;
}

const AddLanguage = (number) => {
  ReadCSV("/data/language.csv",function(languages,err){
    console.log("言語を追加するよ");
    let formName = 'skills' + number.toString() +'language';
    let AddlangID = document.getElementById(formName).value;
    let AddlangName = '';
    let i=0;
    while(i < languages.length){
      if(languages[i][0] === AddlangID){
        AddlangName = languages[i][1]
        break;
      }
      i++;
    }
    for(let k=number; k<maxSkillsNum;k++){
      if(k === number) continue;
      let formName = 'skills' + k.toString() +'language';
      let language = document.getElementById(formName);
      language.options[language.length] = new Option(AddlangName,AddlangID);
    }
  });
}

const RemoveLanguage = (langID,number) => {
  console.log("言語を取り除くよ");
  for(let k=number; k<maxSkillsNum;k++){
    if(k === number) continue;
    let formName = 'skills' + k.toString() +'language';
    let language = document.getElementById(formName);
    for(let i=0; i<language.childNodes.length;i++)
      if(langID === language.options[i].value)
        language.removeChild(language.options[i]);
  }
}

const AddFramework = (AddframeID,number) => {
  ReadCSV("/data/framework.csv",function(frameworks,err){
    console.log("フレームワークを追加するよ");
    let formName = 'skills' + number.toString() +'framework';
    let langID = document.getElementById(formName).value;
    let AddframeName = '';
    let i=0;
    while(i < frameworks.length){
      if(frameworks[i][0] === langID && frameworks[i][1] === AddframeID){
        AddframeName = frameworks[i][2];
        break;
      }
      i++;
    }
    for(let k=number; k<maxSkillsNum;k++){
      if(k === number) continue;
      let formName = 'skills' + k.toString() +'framework';
      let framework = document.getElementById(formName);
      framework.options[framework.length] = new Option(AddframeName,AddframeID);
    }
  });
}

const StackRemoveFramework = (frameID,number) => {
  console.log("除去フレームワークをスタック");
  let formName = 'skills' + number.toString() +'language';
  let langID = document.getElementById(formName).value;
  if(typeof RemovedFrameworks !== 'undefined')
    RemovedFrameworks.push([langID,frameID]);
  else{
    RemovedFrameworks = new Array(1);
    RemovedFrameworks[0] = new Array(1);
    RemovedFrameworks[0][0] = langID;
    RemovedFrameworks[0][1] = frameID;
  }
  CheckAllFrameSelected(langID,number);
}

const CheckAllFrameSelected = (langID,number) => {
  ReadCSV("/data/framework.csv",function(frameworks,err){
    let num = 0;
    for(let i=0; i<frameworks.length;i++)
      if(frameworks[i][0] === langID)
        num++;
    let num_selected = 0;
    for(let i=0; i<RemovedFrameworks.length;i++)
      if(RemovedFrameworks[i][0] === langID)
        num_selected++;
    if(num === num_selected)
      RemoveLanguage(langID,number);
  });
}

//languages[i][1]に内容(表示用)．languages[i][0]に対応する数字が入ってます．
const SetUpTag = (number) => {
  ReadCSV("/data/tag.csv",function(tags,err){
    let formName = 'tags' + number.toString();
    let tag = document.getElementById(formName);
    let j = 0;
    removeChildren(tag);
    tag.options[j++] = new Option("指定しない",0);
    for(let i=1;i<tags.length;i++){
      tag.options[j] = new Option(tags[i][1],tags[i][0]);
      j++;
    }
  });
}

const RemoveTag = (tagID,number) => {
  console.log("タグを取り除くよ");
  for(let k=number; k<maxTagsNum;k++){
    if(k === number) continue;
    let formName = 'tags' + k.toString();
    let tag = document.getElementById(formName);
    console.log(tag);
    for(let i=0; i<tag.childNodes.length;i++){
      if(tagID === tag.options[i].value)
        tag.removeChild(tag.options[i]);
    }
  }
}


const removeChildren = (x) =>{
	if (x.hasChildNodes()) {
		while (x.childNodes.length > 0) {
			x.removeChild(x.firstChild)
		}
	}
}

const SetUpPrefecture =() => {
    console.log("Json読み込んだ");
    return fetch(get_prefecture_api, {
        method: 'GET',
        headers:{
            'X-API-KEY':'JwCkL7hOfiGtJbM9lFjoHt0OEA0SV6jDPNiEzqWS',
            'Content-type':'application/json; charset=utf-8'
        },
      }).then((response) => response.json())
          .then((data) => {
            PrefDataSet = data.result;
            PrefDataSet.unshift({prefCode:'0',prefName:"選択されていません"});
            console.log(PrefDataSet);
            var prefecture = document.forms.Form.SelectPrefecture;
            for(var i=0;i<PrefDataSet.length;i++){
                prefecture.options[i] = new Option(PrefDataSet[i].prefName);
            }
    });
}

const SetUpCity = (PrefName) => {
    console.log(SearchPrefCode(PrefName));
    let get_city = get_city_api + "?prefCode=" + SearchPrefCode(PrefName);
    return fetch(get_city, {
        method: 'GET',
        headers:{
            'X-API-KEY':'JwCkL7hOfiGtJbM9lFjoHt0OEA0SV6jDPNiEzqWS',
            'Content-type':'application/json; charset=utf-8'
        },
      }).then((response) => response.json())
          .then((data) => {
            console.log(data.result);
            let city = document.forms.Form.SelectCity;
            city.options[0] = new Option("選択されていません");
            for(var i=0;i<data.result.length;i++){
                city.options[i+1] = new Option(data.result[i].cityName);
            }
    });
}


const SearchPrefCode = (PrefName) =>{
    for(let i=0; i< PrefDataSet.length; i++)
        if(PrefName === PrefDataSet[i].prefName)
            return PrefDataSet[i].prefCode;
}


export { SetUpLanguage, AddLanguage, StackRemoveFramework, RemoveLanguage, AddFramework, SetUpFramework, SetUpTag, RemoveTag, SetUpStation, SetUpPrefecture, SetUpCity };
