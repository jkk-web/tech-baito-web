import firebase from "firebase/app";

import "firebase/auth";
import "firebase/firestore";

// Firebaseの初期化
var config = {
  apiKey: "your key",
  authDomain: "your domain",
  databaseURL: "your url",
  projectId: "your project id",
  storageBucket: "your bucket",
  messagingSenderId: "your sender id",
  appId: "your app id",
  measurementId: "your id"
};

let UserFirebaseProject;
console.log("デバッグいくよ");
//}
try {
  console.log("どりゃああ！！！");
  UserFirebaseProject = firebase.initializeApp(config,"UserProject");
  console.log("でけた！");
} catch (err) {
  console.log("既にappが存在するのでパスします");
}
// アカウント登録
const signUp = (email, password, callback) => {
  UserFirebaseProject.auth().createUserWithEmailAndPassword(email, password)
    .then(result => {
      console.log(result);
      alert("サインアップ成功");
      callback(result);
    })
    .catch(error => {
      console.log("signup error");
      let errorCode = error.code;
      let errorMessage = error.message;
      // TODO: アラートじゃなくてエラーメッセージにする
      if (
        errorMessage ===
        "The email address is already in use by another account."
      ) {
        alert("サインアップ失敗:このメールアドレスはすでに使用されています");
      } else {
        console.log(errorMessage);
        alert("サインアップ失敗: " + errorCode + ", " + errorMessage);
      }
    });
};

// ログイン
const signIn = (email, password, callback) => {
  UserFirebaseProject
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then(result => {
      callback(result);
    })
    .catch(error => {
      let errorCode = error.code;
      let errorMessage = error.message;
      alert("サインイン失敗: " + errorCode + ", " + errorMessage);
    });
};

// ログアウト
const signOut = () => {
  UserFirebaseProject
    .auth()
    .signOut()
    .then(function() {
      // 特に何もしない
    })
    .catch(function(error) {
      console.log(error);
      alert("サインアウト失敗");
    });
};

    // ログイン確認
const confirmSignined = (callback) =>{
  UserFirebaseProject.auth().onAuthStateChanged(function(user) {
    if (user) {
      // ログイン認証済
      callback(user.uid);
    } else {
      // No user is signed in.
      console.log("未ログインです");
      callback("-1");
    }
  });
}

export { signUp, signIn, signOut, confirmSignined };
