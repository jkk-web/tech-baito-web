import React, { useState } from "react";
import { useRouter } from "next/router";
import { useDispatch } from "react-redux";

import { setUser } from "@/redux/actions/userActions";
import { signIn } from "@/function/firebase-auth-user";

import Layout from "@/components/molecules/Layout";
import Card from "@/components/atoms/Card";
import TextInput from "@/components/atoms/forms/TextInput";
import Submit from "@/components/atoms/forms/Submit";
import Loading from "@/components/atoms/Loading";
import OvalButton from "@/components/atoms/buttons/OvalButton";

// TODO: ログインの処理実装とか
const Page = () => {
  // redux
  const dispatch = useDispatch();
  // router
  const router = useRouter();
  // 入力を保存するstate
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  // ローディングのstate
  const [isLoading, setIsLoading] = useState(false);

  // 入力が変更されたときの処理
  const handleChange = event => {
    const name = event.target.name;
    const value = event.target.value;
    if (name === "email") {
      setEmail(value);
      return;
    }
    if (name === "password") {
      setPassword(value);
      return;
    }
  };
  // form送信の処理
  const handleSubmit = event => {
    // ページのリロードを防ぐ
    event.preventDefault();
    // ローディング
    setIsLoading(true);
    signIn(email, password, function(user, error) {
      if (error) {
        // TODO: エラー時の処理
      }
      //firebaseの企業用DBにemailとパスワードでサインアップ
      let userID = user.user.uid;
      // reduxにdispatch --> userIDをセット
      dispatch(setUser(userID));
      // ページ遷移; とりあえずマイページに飛ばす
      router.push("/mypage");
    });
  };

  return (
    <Layout>
      <main>
        <h1>ログイン</h1>
        <Card width="60%" center>
          <section>
            <form onSubmit={handleSubmit}>
              <label htmlFor="email">
                メールアドレス / user ID
                <TextInput
                  required
                  placeholder="hoge@mail.com"
                  type="email"
                  name="email"
                  value={email}
                  onChange={handleChange}
                />
              </label>
              <label>
                パスワード
                <TextInput
                  type="password"
                  required
                  placeholder="パスワードを入力してください"
                  name="password"
                  value={password}
                  onChange={handleChange}
                />
              </label>
              <div className="submit-wrapper">
                {isLoading ? (
                  <OvalButton disabled width="10rem">
                    <Loading size="16" />
                  </OvalButton>
                ) : (
                  <Submit width="10rem" value="ログイン" />
                )}
              </div>
            </form>
          </section>
        </Card>
      </main>
      <style jsx>{`
        form {
          display: flex;
          flex-direction: column;
        }
        label {
          margin-top: 1rem;
          display: flex;
          justify-content: space-between;
        }
        main {
          display: flex;
          flex-direction: column;
          align-items: center;
          width: 100%;
        }
        .submit-wrapper {
          display: flex;
          justify-content: center;
          margin-top: 2rem;
        }
      `}</style>
    </Layout>
  );
};

export default Page;
