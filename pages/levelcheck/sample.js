import React from "react";
import Link from "next/link";

import Layout from "@/components/molecules/Layout";
import BoxText from "@/components/atoms/BoxText";
import SubmitForm from "@/components/organisms/SubmitForm";
import Card from "@/components/atoms/Card";

const Sample = () => {
  return (
    <Layout>
      <main>
        <section>
          <div className="title">
            <BoxText>課題</BoxText>
            「ポートフォリオページを作成せよ」
          </div>
          ヒント:
          <ul>
            <li>HTML, CSS, JSを使う</li>
            <li>オリジナリティを出そう</li>
          </ul>
        </section>
        <section>
          <div className="title">
            <BoxText>おすすめ教材</BoxText>
          </div>
          <div>
            <div>
              <p>HTMLとCSSの基礎を勉強するにはこの本がおすすめ！</p>
              <Link href="https://www.amazon.co.jp/dp/4797393157">
                <a>https://www.amazon.co.jp/dp/4797393157</a>
              </Link>
            </div>
            <div>
              <p>動画が勉強しやすい人はこっちがおすすめ！</p>
              {/* TODO: OGPでサムネイル表示させたい */}
              <Link href="https://youtu.be/WYDzyIgOezQ">
                <a>https://youtu.be/WYDzyIgOezQ</a>
              </Link>
            </div>
          </div>
        </section>
        <section>
          <div className="title">
            <BoxText>おすすめツール</BoxText>
          </div>
          <ul>
            <li>
              <Link href="https://qiita.com/turmericN/items/37c3fbe00ee6bbc7e201">
                <a>エンジニア向けのポートフォリオサイトまとめ</a>
              </Link>
            </li>
            <li>Qiitaとか</li>
            <li>Qiitaとか</li>
          </ul>
        </section>
        <Card>
          <SubmitForm />
        </Card>
      </main>
      <style jsx>{`
        main {
          display: flex;
          flex-direction: column;
          align-items: center;
        }
        section {
          margin-bottom: 2rem;
          align-self: flex-start;
        }

        .title {
          display: flex;
          align-items: center;
          margin-bottom: 1rem;
        }
      `}</style>
    </Layout>
  );
};
export default Sample;
