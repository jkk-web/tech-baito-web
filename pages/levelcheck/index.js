import React, { useState } from "react";
import { useRouter } from "next/router";

import Layout from "@/components/molecules/Layout";
import SelectInput from "@/components/atoms/forms/SelectInput";
import Submit from "@/components/atoms/forms/Submit";
// レベル
const levels = [...Array(5)].map((item, i) => i + 1);
// 分野
const categoryValues = ["react", "vue", "angular"];
const categoryTexts = ["React", "Vue", "Angular"];

const Page = () => {
  // router
  const router = useRouter();
  // 分野、レベルをstateで管理
  const [category, setCategory] = useState("react");
  const [level, setLevel] = useState(1);

  const handleChange = event => {
    // セレクトボックスの名前を取得
    const name = event.target.name;
    // 選択された値を取得
    const value = event.target.value;
    // stateに値を設定
    if (name === "category") {
      setCategory(value);
      return;
    }
    if (name === "level") {
      setLevel(value);
    }
  };

  const handleSubmit = event => {
    // ↓これがないと、遷移したあとにまたこのページに戻ってきちゃう
    event.preventDefault();
    console.log(`分野: ${category}, レベル: ${level}`);
    // 選択された分野、レベルの値をクエリに入れたページに遷移
    router.push({
      pathname: "/levelcheck/sample",
      query: {
        category,
        level
      }
    });
  };

  return (
    <Layout>
      <h1>問題・課題に挑戦する</h1>
      <form onSubmit={handleSubmit}>
        <label htmlFor="category">
          分野
          <div className="select-wrapper">
            <SelectInput
              name="category"
              value={category}
              values={categoryValues}
              texts={categoryTexts}
              onChange={handleChange}
            />
          </div>
        </label>

        <label htmlFor="level">
          レベル
          <div className="select-wrapper">
            <SelectInput
              name="level"
              onChange={handleChange}
              value={level}
              values={levels}
              texts={levels}
            />
          </div>
        </label>
        <Submit value="問題に挑戦する" />
      </form>
      <style jsx>{`
        form {
          display: flex;
          flex-direction: row;
          align-items: center;
          width: 60%;
        }
        label {
          display: flex;
          flex-direction: row;
          align-items: center;
          margin-left: 1rem;
          width: 100%;
        }
        .select-wrapper {
          margin-left: 0.5rem;
        }
      `}</style>
    </Layout>
  );
};
export default Page;
