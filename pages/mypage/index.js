import React from "react";

import { useSelector, useDispatch } from "react-redux";

import Layout from "@/components/molecules/Layout";
import Alerts from "@/components/pages/mypage/Alerts";
import Info from "@/components/pages/mypage/Info";
import Applications from "@/components/pages/mypage/Applications";
import Card from "@/components/atoms/Card";

const Page = () => {
  const { userID } = useSelector(state => state.user);
  console.log("mypage says:" + userID);
  return (
    <Layout>
      <h1>マイページ</h1>
      <div className="card-wrapper">
        <Card>
          <Alerts userID={userID} />
        </Card>
      </div>
      <div className="card-wrapper">
        <Card>
          <Info userID={userID} />
        </Card>
      </div>
      <div className="card-wrapper">
        <Card>
          <Applications userID={userID} />
        </Card>
      </div>
      <style jsx>{`
        .card-wrapper {
          margin-bottom: 2rem;
        }
      `}</style>
    </Layout>
  );
};
export default Page;
