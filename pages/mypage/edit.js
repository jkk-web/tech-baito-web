import React, { useState, useEffect } from "react";

import { useSelector } from "react-redux";

import Layout from "@/components/molecules/Layout";
import Card from "@/components/atoms/Card";
import Skill from "@/components/atoms/Skill";
import OvalButton from "@/components/atoms/buttons/OvalButton";
import TextInput from "@/components/atoms/forms/TextInput";
import Textarea from "@/components/atoms/forms/Textarea";
import SelectInput from "@/components/atoms/forms/SelectInput";
import Submit from "@/components/atoms/forms/Submit";

import { SetUpPrefecture, SetUpCity, SetUpStation } from "@/function/SetupSelectForm";

const Page = () => {
  // redux
  const UserData = useSelector(state => state.mypage); // mypageのuser_idからでも取れる
  const data = useSelector(state => state.mypage);

  // state: 入力の値を保持する
  const [name, setName] = useState(UserData[0].name);
  const [mail, setMail] = useState(UserData[0].mail);
  const [occupation, setOccupation] = useState(UserData[0].occupation);
  const [prefecture, setPrefecture] = useState(UserData[0].prefecture);
  const [city, setCity] = useState(UserData[0].city);
  const [station, setStation] = useState(UserData[0].station);
  const [portfolio, setPortfolio] = useState(UserData[0].portfolio);
  const [jikopr, setJikopr] = useState(UserData[0].pr);

  // 編集ボタンをクリックしたときの処理
  const handleSubmit = () => {
    // ページのリロードを防ぐ
    event.preventDefault();
    let data = {
      userId: UserData.user_id,
      name: name,
      occupation: occupation,
      prefecture: prefecture,
      city: city,
      station: station,
      portfolio: portfolio,
      pr: jikopr
    };
    let json = JSON.stringify(data);
    console.log(json);

    const user_update_api_url = "http://localhost:4000/api/user/update";

    fetch(user_update_api_url, {
      method: "POST", // or 'PUT'
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify(data) // data can be `string` or {object}!
    }).then(response => {
      if (response.status == 200) alert("ユーザ登録に成功しました");
      else alert("ユーザ登録に失敗しました");
    });
    alert("consoleを見てね");
  };

  // input系が変更されたときの処理
  const handleChange = event => {
    const name = event.target.name;
    const value = event.target.value;

    console.log(UserData[0].name);
    console.log(mail);
    switch (name) {
      case "name":
        setName(value);
        break;
      case "mail":
        //setMail(val);
        alert("メールアドレスは現在変更出来ない仕様になっています");
        break;
      case "occupation":
        setOccupation(value);
        break;
      case "prefecture":
        setPrefecture(value);
        SetUpCity(value);
        SetUpStation(value);
        break;
      case "city":
        setCity(value);
        break;
      case "station":
        setStation(value);
        break;
      case "portfolio":
        setPortfolio(value);
        break;
      case "jikopr":
        setJikopr(value);
        break;
    }
  };

  useEffect(() => {
    // 非同期関数を定義
    const f = async () => {
      try {
        SetUpPrefecture();
      } catch (error) {
        console.log(error);
      }
    };
    // 非同期関数呼び出し
    f();
  }, []);

  return (
    <Layout>
      <h1>マイページ</h1>
      <Card center>
        <div className="card-inner">
          <form id="Form" onSubmit={handleSubmit}>
            <label>名前</label>
            <TextInput
              name="name"
              onChange={handleChange}
              placeholder={UserData[0].name}
            />
            <label>メールアドレス</label>
            <TextInput
              name="mail"
              onChange={handleChange}
              placeholder={UserData[0].mail}
            />
            <label>身分</label>
            <SelectInput
              name="occupation"
              width="15rem"
              values={["student", "employed", "housewife"]}
              texts={["学生", "会社員", "主婦"]}
              placeholder={UserData[0].occupation}
              onChange={handleChange}
            />
            <label htmlFor="prefecture">
              都道府県
            <select id="SelectPrefecture"
              required
              name="prefecture"
              value={prefecture}
              placeholder={UserData[0].prefecture}
              onChange={handleChange}
            />
            </label>
            <label htmlFor="city">
              市区町村
            <select id="SelectCity"
              required
              placeholder="△△市"
              name="city"
              value={city}
              onChange={handleChange}
            />
            </label>
            <label htmlFor="station">
              最寄り駅
            <select id="SelectStation"
              required
              placeholder="東京駅"
              name="station"
              value={station}
              onChange={handleChange}
            />
            </label>
            <label>ポートフォリオ</label>
            <TextInput
              name="portfolio"
              type="url"
              onChange={handleChange}
              placeholder={UserData[0].portfolio}
            />
            <label>自己PR</label>
            <Textarea
              name="jikopr"
              onChange={handleChange}
              placeholder={UserData[0].pr}
              width="auto"
            />
            <label>到達レベル</label>
            <div className="skills-container">
              <Skill name="Github" category="common" level={2} />
              <Skill name="React.js" level={3} category="frontend" />
              <Skill name="SASS" level={2} category="frontend" />
              <Skill name="npm/yarn" level={1} category="frontend" />
              <Skill name="PostgreSQL" level={1} category="backend" />
              <Skill name="Nginx" level={1} category="backend" />
            </div>
            <div className="submit-wrapper">
              <Submit />
            </div>
          </form>
        </div>
      </Card>

      <style jsx>{`
        .card-inner {
          width: 60vw;
        }
        form {
          display: flex;
          flex-direction: column;
        }
        label {
          margin-bottom: 0.5rem;
          display: flex;
          align-items: center;
          font-weight: 400;
          font-size: 1.1rem;
        }
        label:not(:first-of-type) {
          margin-top: 3rem;
        }
        .skills-container {
          display: flex;
          flex-flow: row wrap;
        }
        .submit-wrapper {
          display: flex;
          justify-content: center;
          margin-top: 2rem;
        }
      `}</style>
    </Layout>
  );
};
export default Page;
