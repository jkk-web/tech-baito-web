import React, { useCallback, useEffect, useState } from "react";
import Link from "next/link";
import { useDispatch } from "react-redux";

import instance from "@/utils/instance";
import { setAlerts } from "@/redux/actions/alertsActions";

import Layout from "@/components/molecules/Layout";
import Card from "@/components/atoms/Card";
import OvalButton from "@/components/atoms/buttons/OvalButton";
import SearchForm from "@/components/organisms/SearchForm";

import { confirmSignined } from "@/function/firebase-auth-user";

const Page = () => {
  // redux
  const dispatch = useDispatch();
  // state
  const [userID, setUserID] = useState(""); // TODO
  // const [alerts, setAlerts] = useState([]);

  useEffect(() => {
    // 非同期関数を定義
    // const f = async () => {
    //   try {
    //     // get user id from firebase
    //     const uid = confirmSignined();
    //     console.log(uid);
    //   } catch (error) {
    //     console.log(error);
    //   }
    // };
    // // 非同期関数呼び出し
    // f();

    // TODO: userIDをとってくる
    // APIから通知をとってくる
    const getAlerts = async () => {
      const res = await instance.get(`applications/find?user_id=${userID}`);
      // setAlerts(res.data);
      dispatch(setAlerts(res.data));
    };
    getAlerts();
  }, []);

  return (
    <Layout>
      <main>
        <Card width="60%" center>
          <section className="about">
            <h1>TECH-BAITOとは</h1>
            <p>
              TECH-BAITOとは、プログラミングのバイト先を探している学生や社会人と、バイトを雇いたい企業をつなぐサービスです。
              説明などなど説明などなど説明などなど説明などなど説明などなど
            </p>
            <div className="buttons-container">
              アカウント作成・ログインはここから↓
              <div className="buttons-wrapper">
                <Link href="/signup">
                  <a>
                    <OvalButton>アカウント作成</OvalButton>
                  </a>
                </Link>
                <Link href="/login">
                  <a>
                    <OvalButton>ログイン</OvalButton>
                  </a>
                </Link>
              </div>
            </div>
            <div className="company-wrapper">
              企業の方は
              <b>
                <Link href="/company">
                  <a>こちらのページ</a>
                </Link>
              </b>
              をご利用ください
            </div>
          </section>
        </Card>
        <section className="search">
          <Card>
            <h2>バイトを探す</h2>
            <SearchForm />
          </Card>
        </section>
        <style jsx>{`
          main {
            /* flex */
            display: flex;
            flex-direction: column;
            align-items: center;
          }
          p {
            text-align: center;
          }
          .about {
            /* flex */
            display: flex;
            flex-flow: column;
            align-items: center;
            flex-grow: 0;
          }
          .buttons-container {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin-top: 1rem;
          }
          .buttons-wrapper {
            display: flex;
            justify-content: space-between;
            width: 120%;
            margin-top: 0.5rem;
          }
          .company-wrapper {
            margin-top: 3rem;
          }
          .search {
            margin-top: 5rem;
            width: 60vw;
          }
          .search h1 {
            align-self: center;
          }
        `}</style>
      </main>
    </Layout>
  );
};

export default Page;
