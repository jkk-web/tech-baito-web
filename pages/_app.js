import React from "react";
import App, { AppInitialProps } from "next/app";
import withRedux, { AppProps } from "next-redux-wrapper";
import { Provider } from "react-redux";

import { makeStore } from "@/redux/store";
import "@/styles.css";

export default withRedux(makeStore)(
  class MyApp extends App {
    static async getInitialProps({ Component, ctx }) {
      const pageProps = Component.getInitialProps
        ? await Component.getInitialProps(ctx)
        : {};

      return { pageProps };
    }

    render() {
      const { Component, pageProps, store } = this.props;

      return (
        <>
          <Provider store={store}>
            <Component {...pageProps} />
          </Provider>
        </>
      );
    }
  }
);
/* @see: https://nextjs.org/docs/basic-features/built-in-css-support */
/*
const App = ({ Component, pageProps }) => {
  return <Component {...pageProps} />;
};

export default App;
*/
