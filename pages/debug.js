import React from "react";
import Link from "next/link";
import Layout from "@/components/molecules/Layout";

const routes = [
  {
    path: "/",
    title: "トップページ"
  },
  {
    path: "/levelcheck",
    title: "レベルチェック | TOP"
  },
  {
    path: "/levelcheck/sample",
    title: "レベルチェック | サンプル"
  },
  { path: "/login", title: "ログイン" },
  {
    path: "/mypage",
    title: "マイページ"
  },
  {
    path: "/search",
    title: "求人一覧"
  },
  {
    path: "/search/sample",
    title: "株式会社Foo"
  },
  { path: "/signup", title: "アカウント作成" },
  // ここから企業のやつ
  {
    path: "/company",
    title: "企業TOP"
  },
  {
    path: "/company/login",
    title: "ログイン | 企業"
  },
  {
    path: "/company/applicants",
    title: "応募者一覧 | 企業"
  },
  {
    path: "/company/applicants/pass",
    title: "応募者一覧 (合格) | 企業"
  },
  {
    path: "/company/applicants/fail",
    title: "応募者一覧 (不合格) | 企業"
  },
  {
    path: "/company/post",
    title: "求人を出す | 企業"
  },
  {
    path: "/company/signup",
    title: "アカウント作成 | 企業"
  }
];
const Page = () => {
  return (
    <Layout>
      <h1>画面一覧</h1>
      <ul>
        {routes.map(item => (
          <li key={item.title}>
            <Link href={item.path}>
              <a>{item.title}</a>
            </Link>
          </li>
        ))}
      </ul>
    </Layout>
  );
};

export default Page;
