import React from "react";
import Layout from "@/components/molecules/Layout";

const Page = () => {
  return (
    <Layout>
      <main></main>
      <style jsx>{`
        /* スタイルはここに書いてね */
        main {
          /* 基本はこんな感じ; 必要に応じてflex-directionとか変えてね */
          width: 100%;
          display: flex;
          flex-direction: column;
          align-items: center;
        }
      `}</style>
    </Layout>
  );
};
export default Page;
