import React from "react";


import Layout from "@/components/molecules/Layout";
import Card from "@/components/atoms/Card";
import SearchForm from "@/components/organisms/SearchForm";

const Page = () => {

  return (
    <Layout>
      <section className="search">
        <Card>
          <h2>バイトを探す</h2>
          <SearchForm />
        </Card>
      </section>
      <style jsx>{`
        .card-wrapper {
          margin-bottom: 2rem;
        }
      `}</style>
    </Layout>
  );
};
export default Page;
