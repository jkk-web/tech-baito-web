import React from "react";
import { useSelector } from "react-redux";

import instance from "@/utils/instance";

import Layout from "@/components/molecules/Layout";
import Card from "@/components/atoms/Card";
import OvalButton from "@/components/atoms/buttons/OvalButton";
import SubmitForm from "@/components/organisms/SubmitForm";
import {
  faBuilding,
  faYenSign,
  faMapMarkerAlt,
  faCalendarDay,
  faDesktop
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Page = () => {
  // redux
  const data = useSelector(state => state.recruit);
  const { userID } = useSelector(state => state.user);
  // 応募をクリックしたときの処理
  // TODO: handleClick渡す
  const handleClick = async () => {
    const recruitID = data.recruit_id;
    const res = await instance.post("/applications/insert", {
      recruit_id: recruitID,
      user_id: userID,
      state: 0 // 待ちの状態
    });
    if (res.status === 200) {
      const applyID = res.data.apply_id;
      alert(`応募IDは${applyID}です`);
    } else {
      // エラー
      console.log(res);
    }
  };

  return (
    <Layout>
      <main>
        <h1>{data.title}</h1>
        <div className="card-wrapper">
          <Card>
            <h2>基本情報</h2>
            <dl>
              <dt>
                <div className="icon-wrapper">
                  <FontAwesomeIcon icon={faBuilding} />
                </div>
                会社名
              </dt>
              <dd>TODO</dd>
              <dt>
                <div className="icon-wrapper">
                  <FontAwesomeIcon icon={faMapMarkerAlt} />
                </div>
                都道府県
              </dt>
              <dd>{data.prefecture}</dd>
              <dt>
                <div className="icon-wrapper">
                  <FontAwesomeIcon icon={faYenSign} />
                </div>
                時給
              </dt>
              <dd>{data.wage}円</dd>
              <dt>
                <div className="icon-wrapper">
                  <FontAwesomeIcon icon={faCalendarDay} />
                </div>
                働く日数/日
              </dt>
              <dd>週{data.day_num}日以上</dd>
              <dt>
                <div className="icon-wrapper">
                  <FontAwesomeIcon icon={faDesktop} />
                </div>
                リモートワーク
              </dt>
              <dd>{data.remote === 1 ? "可" : "不可"}</dd>
            </dl>
          </Card>
        </div>
        <div className="card-wrapper">
          <Card>
            <h2>説明</h2>
            {data.explain}
          </Card>
        </div>
        <OvalButton onClick={handleClick}>応募する</OvalButton>
        {/* <div className="card-wrapper">
          <Card>
            <h2>特別課題</h2>
            <h3>RESAS APIを使ってグラフを描画するWebアプリケーションの作成</h3>
            要件
            <ul>
              <li>Reactまたはその他のJavaScriptフレームワーク</li>
              <li>RESAS API</li>
              <li>最新版のGoogle Chromeで動作</li>
            </ul>
            <div className="form-wrapper">
              <SubmitForm />
            </div>
          </Card>
        </div> */}
      </main>
      <style jsx>{`
        dl {
          display: flex;
          flex-flow: row wrap;
          align-items: center;
          margin: 1rem 0 0;
        }
        dt:not(:last-of-type),
        dd:not(:last-of-type) {
          margin-bottom: 2rem;
        }
        dt {
          /*  border-bottom: 2px dashed rgb(155, 194, 207); */
          display: flex;
          flex-basis: 10rem;
          font-weight: 400;
          min-height: 1.2rem;
        }
        dd {
          margin-left: 0;
          flex-basis: calc(60vw - 2rem - 10rem);
          min-height: 1.2rem;
          flex-grow: 1;
        }
        main {
          width: 100%;
          display: flex;
          flex-direction: column;
          align-items: center;
        }
        .card-wrapper {
          margin-bottom: 2rem;
          width: 70vw;
        }
        .icon-wrapper {
          font-weight: 300;
          margin-right: 0.5rem;
        }
        .form-wrapper {
          margin-top: 2rem;
          padding: 1rem;
          border: 2px dashed lightblue;
        }
      `}</style>
    </Layout>
  );
};
export default Page;
