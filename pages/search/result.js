import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";

import instance from "@/utils/instance";
import { setRecruit } from "@/redux/actions/recruitActions";

import { AnalyzeSearchResult } from "@/function/AssistSearchFunc";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faMapMarkerAlt,
  faYenSign,
  faCalendarDay,
  faDesktop
} from "@fortawesome/free-solid-svg-icons";

import Layout from "@/components/molecules/Layout";
import Card from "@/components/atoms/Card";
import Skill from "@/components/atoms/Skill";
import MyPagination from "@/components/organisms/MyPagination";

const Page = () => {
  // redux
  const dispatch = useDispatch();
  const searchData = useSelector(state => state.search); // 検索の設定
  // router
  const router = useRouter();
  // APIのデータ
  const [RecruitData, setRecruitData] = useState([]);

  const [activePage, setActivePage] = useState(1);
  const [AppearData, setAppearData] = useState([]);

  const NumPerPage = 5;
  let TotalDataNum = 0;
  // APIからskillsを取得
  // componentのmount時のみ実行
  useEffect(() => {
    console.log(JSON.stringify(searchData));
    let queryData = {
      prefecture:searchData.prefecture,
      city:searchData.city,
      station:searchData.station,
      minWage:searchData.minWage,
      maxWage:searchData.maxWage,
      minSift:searchData.minSift,
      maxSift:searchData.maxSift,
      remote:searchData.remote,
    };
    console.log(queryData)
    // 非同期関数を定義
    const f = async () => {
      try {
        const res = await instance.post("/recruitment/get_user_required", {
          ...queryData
        });
        console.log(res.data);
        let data = res.data.data; // レスポンスの中の必要なやつだけ
        data = AnalyzeSearchResult(data,searchData.skills,searchData.tags);
        console.log(data)
        setRecruitData(data);
        setAppearData(
          data.slice(NumPerPage * (activePage - 1), NumPerPage * activePage)
        );
        if(data != null) TotalDataNum = data.length;
      } catch (error) {
        console.log(error);
      }
    };
    // 非同期関数呼び出し
    f();
  }, []);

  const handlePageChange = pageNumber => {
    console.log(pageNumber);
    setActivePage(pageNumber);
    setAppearData(
      RecruitData.slice(NumPerPage * (pageNumber - 1), NumPerPage * pageNumber)
    );
  };

  // カードをクリックしたときの処理
  const handleClick = recruit_id => {
    // idに該当する求人
    const item = RecruitData.find(item => item.recruit_id === recruit_id);
    // reduxにデータをセット
    dispatch(setRecruit(item));
    // 詳細ページに遷移
    router.push(`/search/[id]`, `/search/${recruit_id}`);
  };

  return (
    <Layout>
      <main>
        <h1>求人一覧</h1>

        {AppearData.map((item, i) => (
          <div
            key={i}
            className="card-wrapper"
            onClick={() => handleClick(item.recruit_id)}
          >
            <Card>
              <section>
                <h2>{item.title}</h2>

                <div className="card-inner">
                  <img src={item.picture} />
                  <div className="card-inner-right">
                    <div className="skills-container">
                      <Skill name="Github" category="common" level={2} />
                      <Skill name="React.js" level={3} category="frontend" />
                      <Skill name="SASS" level={2} category="frontend" />
                      <Skill name="npm/yarn" level={1} category="frontend" />
                      <Skill name="PostgreSQL" level={1} category="backend" />
                      <Skill name="Nginx" level={1} category="backend" />
                    </div>
                    <article>{item.explain}</article>
                    {/* とりあえずトップページの「バイトを探す」条件と統一 */}
                    <div className="info">
                      <div className="info-item">
                        <div className="icon-wrapper">
                          <FontAwesomeIcon icon={faMapMarkerAlt} />
                        </div>
                        東京都新宿区
                      </div>
                      <div className="info-item">
                        <div className="icon-wrapper">
                          <FontAwesomeIcon icon={faYenSign} />
                        </div>
                        {item.wage}円
                      </div>
                      <div className="info-item">
                        <div className="icon-wrapper">
                          <FontAwesomeIcon icon={faCalendarDay} />
                        </div>
                        週{item.day_num}日〜
                      </div>
                      <div className="info-item">
                        <div className="icon-wrapper">
                          <FontAwesomeIcon icon={faDesktop} />
                        </div>
                        {item.remote === "1" ? "リモート可" : "リモート不可"}
                      </div>
                    </div>
                  </div>
                </div>
                <h3>{item.co_name}</h3>
              </section>
            </Card>
          </div>
        ))}
        <div>
          <MyPagination
            activePage={activePage}
            itemsCountPerPage={NumPerPage}
            totalItemsCount={TotalDataNum}
            pageRangeDisplayed={5}
            onChange={handlePageChange}
          />
        </div>
      </main>
      <style jsx>{`
        article {
          margin-bottom: 1rem;
        }
        h2 {
          text-align: left;
        }
        h3 {
          margin: 1rem 0 0;
          text-align: right;
          color: gray;
        }
        main {
          width: 100%;
          display: flex;
          flex-direction: column;
          align-items: center;
        }
        img {
          height: 10vw;
          margin-right: 2rem;
        }
        .card-inner {
          display: flex;
          width: 60vw;
          margin-top: 1rem;
        }
        .card-inner-right {
          /* flex */
          flex-basis: 80%;
          flex-direction: column;
          /* size */
          margin-left: 1rem;
        }

        .card-wrapper {
          margin-bottom: 2rem;
        }
        .card-wrapper:hover {
          cursor: pointer;
        }
        .info {
          display: flex;
          justify-content: space-between;
          flex-flow: row wrap;
        }
        .info-item {
          display: flex;
          align-items: center;
          margin-right: 2rem;
        }
        .icon-wrapper {
          width: 1.5rem;
          text-align: center;
          margin-right: 0.5rem;
          font-size: 1.5rem;
        }
        .skills-container {
          display: flex;
          flex-flow: row wrap;
          height: fit-content;
        }
      `}</style>
    </Layout>
  );
};
export default Page;
