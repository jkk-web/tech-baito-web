import React, { useEffect,  useState } from "react";

import Layout from "@/components/molecules/Layout";
import Card from "@/components/atoms/Card";
import TextInput from "@/components/atoms/forms/TextInput";
import SelectInput from "@/components/atoms/forms/SelectInput.js";
import Submit from "@/components/atoms/forms/Submit";

import { signUp } from "@/function/firebase-auth-co";
import { SetUpPrefecture, SetUpCity, SetUpStation } from "@/function/SetupSelectForm";

// TODO: ログインの処理実装とか
const Page = () => {
  // 入力を保存するstate
  const [companyName, setCompanyName] = useState("");
  const [Department, setDepartment] = useState("");
  const [Name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [tel, setTel] = useState("");
  const [Fax, setFax] = useState("");
  const [Postal, setPostal] = useState("");
  const [Prefecture, setPrefecture] = useState("");
  const [City, setCity] = useState("");
  const [address, setAddress] = useState("");
  const [station, setStation] = useState("");
  const [url, setUrl] = useState("");
  const [password, setPassword] = useState("");
  // 入力が変更されたときの処理
  const handleChange = event => {
    const name = event.target.name;
    const value = event.target.value;
    switch(name){
      case "companyName":
        setCompanyName(value);
        break;
      case "Department":
        setDepartment(value);
        break;
      case "Name":
        setName(value);
        break;
      case "email":
        setEmail(value);
        break;
      case "tel":
        setTel(value);
        break;
      case "Fax":
        setFax(value);
        break;
      case "Postal":
        setPostal(value);
        break;
      case "Prefecture":
        setPrefecture(value);
        SetUpCity(value);
        SetUpStation(value);
        break;
      case "City":
        setCity(value);
        break;
      case "address":
        setAddress(value);
        break;
      case "station":
        setStation(value);
        break;
      case "password":
        setPassword(value);
        break;
      case "tel":
        setTel(value);
        break;
      case "url":
        setUrl(value);
        break;
      case "occupation":
        setOccupation(value);
        break;
    }
  };

  useEffect(() => {
    // 非同期関数を定義
    const f = async () => {
      try {
        SetUpPrefecture();
      } catch (error) {
        console.log(error);
      }
    };
    // 非同期関数呼び出し
    f();
  }, []);

  // form送信の処理
  const handleSubmit = event => {
    // ページのリロードを防ぐ
    event.preventDefault();
    if(City === ""){
      alert("市区町村を設定してください");
      return;
    }

    signUp(email, password, function(company, error) {
      //firebaseの企業用DBにemailとパスワードでサインアップ
      let companyID = company.user.uid; //uidにユーザidが入ってます！
      let data = {
        co_id: companyID,
        name: companyName,
        department: Department,
        representative: Name,
        email: email,
        tel: tel,
        fax: Fax,
        postal: Postal,
        prefecture: Prefecture,
        city: City,
        address: address,
        station: station,
        url: url
      };
      let json = JSON.stringify(data);
      console.log(json);

      const com_signup_api_url = "http://localhost:4000/api/company/insert";

      fetch(com_signup_api_url, {
        method: "POST", // or 'PUT'
        headers: {
          "Content-type": "application/json"
        },
        body: JSON.stringify(data) // data can be `string` or {object}!
      }).then(response => {
        if (response.status == 200) alert("ユーザ登録に成功しました");
        else alert("ユーザ登録に失敗しました");
      });
    });
  };

  return (
    <Layout>
      <main>
        <Card width="60%" center>
          <section>
            <h1>企業登録申請フォーム</h1>
            <form id="Form" onSubmit={handleSubmit}>
              <label htmlFor="companyName">
                企業名
                <TextInput
                  required
                  placeholder="〇〇株式会社"
                  type="text"
                  name="companyName"
                  value={companyName}
                  onChange={handleChange}
                />
              </label>
              <label htmlFor="Department">
                部署名
                <TextInput
                  required
                  placeholder="××部門"
                  type="text"
                  name="Department"
                  value={Department}
                  onChange={handleChange}
                />
              </label>
              <label htmlFor="Name">
                担当者名
                <TextInput
                  required
                  placeholder="バイト　太郎"
                  type="text"
                  name="Name"
                  value={Name}
                  onChange={handleChange}
                />
              </label>
              <label htmlFor="email">
                メールアドレス(企業)
                <TextInput
                  required
                  placeholder="hoge@mail.com"
                  type="email"
                  name="email"
                  value={email}
                  onChange={handleChange}
                />
              </label>
              <label htmlFor="tel">
                電話番号(企業)
                <TextInput
                  required
                  name="tel"
                  type="tel"
                  placeholder="080-1111-2222"
                  value={tel}
                  onChange={handleChange}
                />
              </label>
              <label htmlFor="Fax">
                Fax(企業)
                <TextInput
                  placeholder="090000000"
                  type="text"
                  name="Fax"
                  value={Fax}
                  onChange={handleChange}
                />
              </label>
              <label htmlFor="Postal">
                郵便番号
                <TextInput
                  required
                  pattern="\d{3}-\d{4}"
                  placeholder="000-0000"
                  type="text"
                  name="Postal"
                  value={Postal}
                  onChange={handleChange}
                />
              </label>
              <label htmlFor="Prefecture">
                都道府県
                <select id="SelectPrefecture"
                  required
                  name="Prefecture"
                  value={Prefecture}
                  onChange={handleChange}
                />
              </label>
              <label htmlFor="City">
                市区町村
                <select id="SelectCity"
                  required
                  placeholder="△△市"
                  name="City"
                  value={City}
                  onChange={handleChange}
                />
              </label>
              <label htmlFor="adress">
                番地・ビル名・号室など
                <TextInput
                  required
                  placeholder="○○番地□□ビル××号室"
                  type="text"
                  name="address"
                  value={address}
                  onChange={handleChange}
                />
              </label>
              <label htmlFor="station">
                最寄り駅
                <select id="SelectStation"
                  required
                  placeholder="東京駅"
                  name="station"
                  value={station}
                  onChange={handleChange}
                />
              </label>
              <label htmlFor="url">
                貴社ホームページURL
                <TextInput
                  placeholder="http://"
                  type="url"
                  name="url"
                  value={url}
                  onChange={handleChange}
                />
              </label>
              <label htmlFor="password">
                パスワード
                <TextInput
                  required
                  name="password"
                  type="password"
                  placeholder="パスワードを入力してください"
                  value={password}
                  onChange={handleChange}
                />
              </label>
              <div className="submit-wrapper">
                <Submit />
              </div>
            </form>
          </section>
        </Card>
      </main>
      <style jsx>{`
        h1 {
          margin: 0;
        }
        form {
          display: flex;
          flex-direction: column;
        }
        label {
          margin-top: 1rem;
          display: flex;
          justify-content: space-between;
        }
        main {
          display: flex;
          flex-direction: column;
          align-items: center;
          width: 100%;
        }
        .submit-wrapper {
          display: flex;
          justify-content: center;
          margin-top: 2rem;
        }
      `}</style>
    </Layout>
  );
};

export default Page;
