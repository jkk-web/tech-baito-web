import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import Layout from "@/components/molecules/Layout";
import Card from "@/components/atoms/Card";
import Button from "@/components/atoms/buttons/Button";
import { useRouter } from "next/router";
import { useDispatch } from "react-redux";
import { setRecruit } from "@/redux/actions/recruitActions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import instance from "@/utils/instance";
// import Skill from "@/components/atoms/Skill";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { faUsers } from "@fortawesome/free-solid-svg-icons";
// import { faCalendarAlt } from "@fortawesome/free-regular-svg-icons";
const Page = () => {
  // redux
  const dispatch = useDispatch();
  // router
  const router = useRouter();

  //const CompanyID = 'hHFjOTV5SDSiQ4bakEk2UR08pHg1';
  const { CompanyID } = useSelector(state => state.company);
  const [UsersNum, setUsersNum] = useState([0,0,0]);
  const [RecruitData, setRecruitData] = useState([]);

  useEffect(() => {
    // 非同期関数を定義
    const f = async () => {
      try {
        const res = await instance.get(`recruitment/collect?companyID=${CompanyID}`);
        setRecruitData(res.data);
        countUsers(res.data);
      } catch (error) {
        console.log(error);
      }
    };
    // 非同期関数呼び出し
    f();
  }, []);

  // カードをクリックしたときの処理
  const handleClick = recruit_id => {
    // idに該当する求人
    const item = RecruitData.find(item => item.recruit_id === recruit_id);
    // reduxにデータをセット
    dispatch(setRecruit(item));
    // 詳細ページに遷移
    router.push(`/search/[id]`, `/search/${recruit_id}`);
  };

  const buttonClick = recruit_id => {
   // idに該当する求人
    const item = RecruitData.find(item => item.recruit_id === recruit_id);
    // reduxにデータをセット
    dispatch(setRecruit(item));
    // 詳細ページに遷移
    router.push(`/company/applicants`);
  };

  // 「求人を削除する」ボタンを押したときの処理
  const handleDelete = () => {
    alert("求人を削除してよろしいですか?");
    // TODO
    // APIにdeleteのリクエスト投げたり
  };

  const countUsers = async (recruits) => {
    var counts = [];
    for(var i=0; i < recruits.length; i++) {
      const res = await instance.get(`applications/count?recruit_id=${recruits[i].recruit_id}`);
      counts.push(res.data[0].count);
    }
    setUsersNum(counts);
  }

  return (
    <Layout>
      <main>
        <h1>募集中の求人</h1>
        <Card>
          <table>
            <thead>
              <tr>
                <th>
                  <div>求人タイトル</div>
                </th>
                <th>
                  <div>応募人数</div>
                </th>
                <th>
                  <div>募集開始日</div>
                </th>
                <th>
                  <div>募集終了日</div>
                </th>
                <th>
                  <div></div>
                </th>
              </tr>
            </thead>
            <tbody>
            {RecruitData.map((item, i) => (
              <tr key={i}>
                <td>
                  <div className="rec_title">
                      <a onClick={() => handleClick(item.recruit_id)}>
                        <b>{item.title}</b>
                      </a>
                  </div>
                </td>
                <td>
                  <div>{UsersNum[i]}人</div>
                </td>
                <td>
                  <div>{item.start_date}</div>
                </td>
                <td>
                 <div>{item.finish_date}</div>
                </td>
                <td>
                  <div>
                    <div className="buttons-container">
                      <div className="button-wrapper">
                        {/* <Link href="/company/recruitments/${item.recruit_id}"> */}
                          <a onClick={() => buttonClick(item.recruit_id)}>
                            <Button>応募者を見る</Button>
                          </a>
                        {/* </Link> */}
                      </div>
                      <div className="button-wrapper">
                        <Button
                          color="salmon"
                          shadowColor="rgb(226, 115, 103)"
                          onClick={handleDelete}
                        >
                          求人を削除する
                        </Button>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            ))}
            </tbody>
          </table>
        </Card>
      </main>
      <style jsx>{`
        /* start table styles */
        table {
          margin-top: 1rem;
          border-collapse: collapse;
        }
        thead > tr {
          text-align: left;
          border-bottom: 2px dashed rgb(155, 194, 207);
        }
        tr {
          border-bottom: 0.1rem solid gray;
        }
        th > div {
          margin-right: 1rem;
        }
        td > div {
          margin: 0.7rem 1rem 0.7rem 0;
        }
        tr:last-child {
          border-bottom: none;
        }
        /* end table styles */
        dl {
          display: flex;
          flex-flow: row wrap;
          margin: 1rem 0 0 0;
        }
        dt {
          flex-basis: 20%;
          font-weight: 400;
        }
        dd {
          flex-basis: 60%;
        }
        h2 {
          text-align: left;
        }
        main {
          display: flex;
          flex-direction: column;
          align-items: center;
        }
        img {
          height: 10vw;
        }
        .buttons-container {
          display: flex;
          justify-content: space-between;
        }
        .button-wrapper {
          margin: 0 0.5rem;
        }
        .card-inner {
          display: flex;
        }
        /*
        .card-wrapper {
          margin: 1rem 0;
        }
        */
        /*
        .info {
          display: flex;
          flex-flow: column;
          margin-left: 1rem;
        }
        */
        .skills-container {
          display: flex;
          flex-flow: row wrap;
        }
        .rec_title {
          cursor: pointer;
        }
      `}</style>
    </Layout>
  );
};
export default Page;