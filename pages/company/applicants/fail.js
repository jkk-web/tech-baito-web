import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import Layout from "@/components/molecules/Layout";
import Tabs from "@/components/organisms/Tabs";
import Card from "@/components/atoms/Card";
import Button from "@/components/atoms/buttons/Button";
import { faHeart, faHeartBroken } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Page = () => {
  const recruit = useSelector(state => state.recruit);
  const [Applicants, setApplicants] = useState([]);

  useEffect(() => {
    // 非同期関数を定義
    const f = () => {
      try {
        getUsers();
      } catch (error) {
        console.log(error);
      }
    };
    // 非同期関数呼び出し
    f();
  }, []);

  const getUsers = async () => {
    let data = {recruit_id: recruit.recruit_id, state: -1};
    const get_users_api_url = "http://localhost:4000/api/applications/get_users";
    await fetch(get_users_api_url, {
      method: 'POST', // or 'PUT'
      headers:{
        'Content-type':'application/json'
      },
      body: JSON.stringify(data), // data can be `string` or {object}!
    }).then((res) => res.json()                                                                                                
    ).then((json) => { 
      console.log(json.data);
      setApplicants(json.data);
    });
  }

  return (
    <Layout>
      <main>
        <h1>応募者一覧 (不合格)</h1>
        <Tabs current={2} />
        {Applicants.map((item, i) => (
          <div 
            className="card-wrapper" 
            key={i}
          >
            <Card>
              <section>
                <h2>{item.name}</h2>
                <div className="card-inner">
                  <img src="/img/kurage.png" />
                  <div className="info">
                    <div className="skill">Node.js | React</div>
                    <dl>
                      <dt>居住地</dt>
                      <dd>{item.prefecture}{item.city}</dd>
                      <dt>職業</dt>
                      <dd>{item.occupation}</dd>
                      {/* <dt>生年月日</dt>
                      <dd>1998年1月31日</dd> */}
                      <dt>ポートフォリオ</dt>
                      <dd>{item.portfolio}</dd>
                    </dl>
                  </div>
                </div>
              </section>
            </Card>
          </div>
        ))};
      </main>
      <style jsx>{`
        h2 {
          text-align: left;
        }
        main {
          /* 基本はこんな感じ; 必要に応じてflex-directionとか変えてね */
          width: 60vw;
          display: flex;
          flex-direction: column;
          align-items: center;
        }
        img {
          height: 10vw;
        }
        /* 基本情報のやつ */
        dl {
          display: flex;
          flex-flow: row wrap;
          margin: 1rem 0 0 0;
        }
        dt {
          flex-basis: 40%;
        }
        dd {
          flex-basis: 50%;
        }

        .buttons-container {
          display: flex;
          justify-content: space-between;
          margin-top: 1rem;
        }
        .card-inner {
          display: flex;
          width: 60vw;
          margin-top: 1rem;
        }
        .card-wrapper {
          margin: 1rem 0;
        }
        .icon-wrapper {
          margin-left: 0.5rem;
        }
        .info {
          display: flex;
          flex-flow: column;
          margin-left: 1rem;
        }
      `}</style>
    </Layout>
  );
};
export default Page;
