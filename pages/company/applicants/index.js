import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import Layout from "@/components/molecules/Layout";
import Tabs from "@/components/organisms/Tabs";
import Card from "@/components/atoms/Card";
import Button from "@/components/atoms/buttons/Button";
import { faHeart, faHeartBroken } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Page = () => {
  const recruit = useSelector(state => state.recruit);
  const [Applicants, setApplicants] = useState([]);

  useEffect(() => {
    // 非同期関数を定義
    const f = () => {
      try {
        getUsers();
      } catch (error) {
        console.log(error);
      }
    };
    // 非同期関数呼び出し
    f();
  }, []);

  const getUsers = async () => {
    let data = {recruit_id: recruit.recruit_id, state: 0};
    const get_users_api_url = "http://localhost:4000/api/applications/get_users";
    await fetch(get_users_api_url, {
      method: 'POST', // or 'PUT'
      headers:{
        'Content-type':'application/json'
      },
      body: JSON.stringify(data), // data can be `string` or {object}!
    }).then((res) => res.json()                                                                                                
    ).then((json) => { 
      console.log(json.data);
      setApplicants(json.data);
    });
  }

  const judge = async (state, user_id) => {
    const text = (state == 1) ? "合格" : "不合格";
    const res = window.confirm(text + "でよろしいですか？");
    if (res) {
      const judge_user_api_url = "http://localhost:4000/api/applications/judge"
      let data = {state: state, recruit_id: recruit.recruit_id, user_id: user_id};
      await fetch(judge_user_api_url, {
        method: 'POST', // or 'PUT'
        headers:{
          'Content-type':'application/json'
        },
        body: JSON.stringify(data), // data can be `string` or {object}!
      }).then(response => {
        if (response.status == 200) {
          getUsers();
        }
        else alert("プッシュ通知の送信に失敗しました");
      });
    }
  }

  return (
    <Layout>
      <main>
        <h1>応募者一覧</h1>
        <Tabs current={0} />
        {Applicants.map((item, i) => (
          <div 
            className="card-wrapper" 
            key={i}
          >
            <Card>
              <section>
                <h2>{item.name}</h2>
                <div className="card-inner">
                  <img src="/img/kurage.png" />
                  <div className="info">
                    <div className="skill">Node.js | React</div>
                    <dl>
                      <dt>居住地</dt>
                      <dd>{item.prefecture}{item.city}</dd>
                      <dt>職業</dt>
                      <dd>{item.occupation}</dd>
                      {/* <dt>生年月日</dt>
                      <dd>****年**月**日</dd> */}
                      <dt>ポートフォリオ</dt>
                      <dd>{item.portfolio}</dd>
                    </dl>
                  </div>
                </div>
                <div className="buttons-container">
                  <Button
                    width="45%"
                    color="salmon"
                    shadowColor="rgb(226, 115, 103)"
                    onClick={() => judge(1, item.user_id)} 
                  >
                    合格
                    <div className="icon-wrapper">
                      <FontAwesomeIcon icon={faHeart} />
                    </div>
                  </Button>
                  <Button
                    width="45%"
                    color="plum"
                    shadowColor="rgb(181, 130, 181)"
                    onClick={() => judge(-1, item.user_id)} 
                  >
                    不合格
                    <div className="icon-wrapper">
                      <FontAwesomeIcon icon={faHeartBroken} />
                    </div>
                  </Button>
                </div>
              </section>
            </Card>
          </div>
        ))}
      </main>
      <style jsx>{`
        main {
          /* 基本はこんな感じ; 必要に応じてflex-directionとか変えてね */
          width: 60vw;
          display: flex;
          flex-direction: column;
          align-items: center;
        }
        h2 {
          text-align: left;
        }
        img {
          height: 10vw;
        }
        /* 基本情報のやつ */
        dl {
          display: flex;
          flex-flow: row wrap;
          margin: 1rem 0 0 0;
        }
        dt {
          flex-basis: 40%;
        }
        dd {
          flex-basis: 50%;
        }

        .buttons-container {
          display: flex;
          justify-content: space-between;
          margin-top: 1rem;
        }
        .card-inner {
          display: flex;
          width: 60vw;
          margin-top: 1rem;
        }
        .card-wrapper {
          margin: 1rem 0;
        }
        .icon-wrapper {
          margin-left: 0.5rem;
        }
        .info {
          display: flex;
          flex-flow: column;
          margin-left: 1rem;
        }
      `}</style>
    </Layout>
  );
};
export default Page;
