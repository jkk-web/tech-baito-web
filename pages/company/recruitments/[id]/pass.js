import React from "react";

import { faCheck, faFlag } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import Layout from "@/components/molecules/Layout";
import Tabs from "@/components/organisms/Tabs";
import Card from "@/components/atoms/Card";
import Button from "@/components/atoms/buttons/Button";
import Filter from "@/components/organisms/Filter";

const Sent = () => {
  return (
    <>
      連絡済み
      <div className="icon-wrapper">
        <FontAwesomeIcon icon={faCheck} />
      </div>
      <style jsx>{`
        .icon-wrapper {
          margin-left: 0.5rem;
        }
      `}</style>
    </>
  );
};

const Page = () => {
  return (
    <Layout>
      <main>
        <h1>応募者一覧 (合格)</h1>
        <Tabs current={1} />
        <Filter />
        <div className="card-wrapper">
          <Card border>
            <section>
              <div className="card-header">
                <h2>ほげた ほげお</h2>
              </div>
              <div className="card-inner">
                <img src="/img/kurage.png" />

                <div className="info">
                  <div className="skill">Node.js | React</div>
                  <dl>
                    <dt>居住地</dt>
                    <dd>東京都調布市</dd>
                    <dt>職業</dt>
                    <dd>大学生</dd>
                    <dt>生年月日</dt>
                    <dd>1998年1月31日</dd>
                    <dt>ポートフォリオ</dt>
                    <dd>http://github.com/hogeta</dd>
                  </dl>
                </div>
              </div>
              <div className="buttons-container">
                <Button width="45%">
                  連絡済みにする
                  <div className="icon-wrapper">
                    <FontAwesomeIcon icon={faCheck} />
                  </div>
                </Button>
                <Button width="45%">
                  採用済みにする
                  <div className="icon-wrapper">
                    <FontAwesomeIcon icon={faFlag} />
                  </div>
                </Button>
              </div>
            </section>
          </Card>
        </div>
        <div className="card-wrapper">
          <Card>
            <section>
              <div className="card-header">
                <h2>ほげた ほげお</h2>
                <Sent />
              </div>
              <div className="card-inner">
                <img src="/img/kurage.png" />

                <div className="info">
                  <div className="skill">Node.js | React</div>
                  <dl>
                    <dt>居住地</dt>
                    <dd>東京都調布市</dd>
                    <dt>職業</dt>
                    <dd>大学生</dd>
                    <dt>生年月日</dt>
                    <dd>1998年1月31日</dd>
                    <dt>ポートフォリオ</dt>
                    <dd>http://github.com/hogeta</dd>
                  </dl>
                </div>
              </div>
              <div className="buttons-container">
                <Button width="45%" disabled>
                  連絡済みにする
                  <div className="icon-wrapper">
                    <FontAwesomeIcon icon={faCheck} />
                  </div>
                </Button>
                <Button width="45%">
                  採用済みにする
                  <div className="icon-wrapper">
                    <FontAwesomeIcon icon={faFlag} />
                  </div>
                </Button>
              </div>
            </section>
          </Card>
        </div>
      </main>
      <style jsx>{`
        main {
          /* 基本はこんな感じ; 必要に応じてflex-directionとか変えてね */
          width: 60vw;
          display: flex;
          flex-direction: column;
          align-items: center;
        }
        h2 {
          text-align: left;
          margin-right: auto;
        }
        img {
          height: 10vw;
          align-self: flex-start;
        }
        /* 基本情報のやつ */
        dl {
          display: flex;
          flex-flow: row wrap;
          margin: 1rem 0 0 0;
        }
        dt {
          flex-basis: 40%;
        }
        dd {
          flex-basis: 50%;
        }

        .buttons-container {
          display: flex;

          justify-content: space-between;
          margin-top: 1rem;
        }
        .card-header {
          display: flex;
          align-items: center;
          width: 100%;
        }
        .card-inner {
          display: flex;
          width: 60vw;
        }
        .card-wrapper {
          margin: 1rem 0;
        }
        .icon-wrapper {
          margin-left: 0.5rem;
        }
        .info {
          display: flex;
          flex-flow: column;
          margin-left: 1rem;
        }
      `}</style>
    </Layout>
  );
};
export default Page;
