import React from "react";
import Layout from "@/components/molecules/Layout";
import Tabs from "@/components/organisms/Tabs";
import Card from "@/components/atoms/Card";
import Button from "@/components/atoms/buttons/Button";
import { faHeart, faHeartBroken } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Page = () => {
  return (
    <Layout>
      <main>
        <h1>応募者一覧</h1>
        <Tabs current={0} />
        <div className="card-wrapper">
          <Card>
            <section>
              <h2>ほげた ほげお</h2>
              <div className="card-inner">
                <img src="/img/kurage.png" />
                <div className="info">
                  <div className="skill">Node.js | React</div>
                  <dl>
                    <dt>居住地</dt>
                    <dd>東京都調布市</dd>
                    <dt>職業</dt>
                    <dd>大学生</dd>
                    <dt>生年月日</dt>
                    <dd>1998年1月31日</dd>
                    <dt>ポートフォリオ</dt>
                    <dd>http://github.com/hogeta</dd>
                  </dl>
                </div>
              </div>
              <div className="buttons-container">
                <Button
                  width="45%"
                  color="salmon"
                  shadowColor="rgb(226, 115, 103)"
                >
                  合格
                  <div className="icon-wrapper">
                    <FontAwesomeIcon icon={faHeart} />
                  </div>
                </Button>
                <Button
                  width="45%"
                  color="plum"
                  shadowColor="rgb(181, 130, 181)"
                >
                  不合格
                  <div className="icon-wrapper">
                    <FontAwesomeIcon icon={faHeartBroken} />
                  </div>
                </Button>
              </div>
            </section>
          </Card>
        </div>
        <div className="card-wrapper">
          <Card>
            <section>
              <h2>ほげた ほげお</h2>
              <div className="card-inner">
                <img src="/img/kurage.png" />
                <div className="info">
                  <div className="skill">Node.js | React</div>
                  <dl>
                    <dt>居住地</dt>
                    <dd>東京都調布市</dd>
                    <dt>職業</dt>
                    <dd>大学生</dd>
                    <dt>生年月日</dt>
                    <dd>1998年1月31日</dd>
                    <dt>ポートフォリオ</dt>
                    <dd>http://github.com/hogeta</dd>
                  </dl>
                </div>
              </div>
              <div className="buttons-container">
                <Button
                  width="45%"
                  color="salmon"
                  shadowColor="rgb(226, 115, 103)"
                >
                  合格
                  <div className="icon-wrapper">
                    <FontAwesomeIcon icon={faHeart} />
                  </div>
                </Button>
                <Button
                  width="45%"
                  color="plum"
                  shadowColor="rgb(181, 130, 181)"
                >
                  不合格
                  <div className="icon-wrapper">
                    <FontAwesomeIcon icon={faHeartBroken} />
                  </div>
                </Button>
              </div>
            </section>
          </Card>
        </div>
        <div className="card-wrapper">
          <Card>
            <section>
              <h2>ほげた ほげお</h2>
              <div className="card-inner">
                <img src="/img/kurage.png" />
                <div className="info">
                  <div className="skill">Node.js | React</div>
                  <dl>
                    <dt>居住地</dt>
                    <dd>東京都調布市</dd>
                    <dt>職業</dt>
                    <dd>大学生</dd>
                    <dt>生年月日</dt>
                    <dd>1998年1月31日</dd>
                    <dt>ポートフォリオ</dt>
                    <dd>http://github.com/hogeta</dd>
                  </dl>
                </div>
              </div>
              <div className="buttons-container">
                <Button
                  width="45%"
                  color="salmon"
                  shadowColor="rgb(226, 115, 103)"
                >
                  合格
                  <div className="icon-wrapper">
                    <FontAwesomeIcon icon={faHeart} />
                  </div>
                </Button>
                <Button
                  width="45%"
                  color="plum"
                  shadowColor="rgb(181, 130, 181)"
                >
                  不合格
                  <div className="icon-wrapper">
                    <FontAwesomeIcon icon={faHeartBroken} />
                  </div>
                </Button>
              </div>
            </section>
          </Card>
        </div>
      </main>
      <style jsx>{`
        main {
          /* 基本はこんな感じ; 必要に応じてflex-directionとか変えてね */
          width: 60vw;
          display: flex;
          flex-direction: column;
          align-items: center;
        }
        h2 {
          text-align: left;
        }
        img {
          height: 10vw;
        }
        /* 基本情報のやつ */
        dl {
          display: flex;
          flex-flow: row wrap;
          margin: 1rem 0 0 0;
        }
        dt {
          flex-basis: 40%;
        }
        dd {
          flex-basis: 50%;
        }

        .buttons-container {
          display: flex;
          justify-content: space-between;
          margin-top: 1rem;
        }
        .card-inner {
          display: flex;
          width: 60vw;
          margin-top: 1rem;
        }
        .card-wrapper {
          margin: 1rem 0;
        }
        .icon-wrapper {
          margin-left: 0.5rem;
        }
        .info {
          display: flex;
          flex-flow: column;
          margin-left: 1rem;
        }
      `}</style>
    </Layout>
  );
};
export default Page;
