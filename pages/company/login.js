import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";

import { setCompany } from "@/redux/actions/companyActions";
import Layout from "@/components/molecules/Layout";
import Card from "@/components/atoms/Card";
import TextInput from "@/components/atoms/forms/TextInput";
import Submit from "@/components/atoms/forms/Submit";

import { signIn } from "@/function/firebase-auth-co";

// TODO: ログインの処理実装とか
const Page = () => {
  // redux
  const dispatch = useDispatch();
  // router
  const router = useRouter();
  // 入力を保存するstate
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  // 入力が変更されたときの処理
  const handleChange = event => {
    const name = event.target.name;
    const value = event.target.value;
    if (name === "email") {
      setEmail(value);
      return;
    }
    if (name === "password") {
      setPassword(value);
      return;
    }
  };
  // form送信の処理
  const handleSubmit = event => {
    event.preventDefault();
    signIn(email, password, function(company, error) {
      if (error) {
        // TODO: エラー時の処理
      }
      //firebaseの企業用DBにemailとパスワードでサインアップ
      let companyID = company.user.uid;
      // reduxに投げる --> companyIDをセット
      dispatch(setCompany(companyID));
      // ページ遷移
      router.push("/company");
    });
  };

  return (
    <Layout>
      <main>
        <Card width="60%" center>
          <section>
            <h1>企業ログイン</h1>
            <form onSubmit={handleSubmit}>
              <label htmlFor="email">
                メールアドレス / user ID
                <TextInput
                  placeholder="hoge@mail.com"
                  type="email"
                  name="email"
                  value={email}
                  onChange={handleChange}
                />
              </label>
              <label>
                パスワード
                <TextInput
                  type="password"
                  required
                  placeholder="パスワードを入力してください"
                  name="password"
                  value={password}
                  onChange={handleChange}
                />
              </label>
              <div className="submit-wrapper">
                <Submit />
              </div>
            </form>
          </section>
        </Card>
      </main>
      <style jsx>{`
        form {
          display: flex;
          flex-direction: column;
        }
        label {
          margin-top: 1rem;
          display: flex;
          justify-content: space-between;
        }
        main {
          display: flex;
          flex-direction: column;
          align-items: center;
          width: 100%;
        }
        .submit-wrapper {
          display: flex;
          justify-content: center;
          margin-top: 2rem;
        }
      `}</style>
    </Layout>
  );
};

export default Page;
