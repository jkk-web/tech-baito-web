import React, { useState, useEffect } from "react";

import { formatISO, parseISO } from "date-fns";

import Layout from "@/components/molecules/Layout";
import Card from "@/components/atoms/Card";
import TextInput from "@/components/atoms/forms/TextInput";
import Textarea from "@/components/atoms/forms/Textarea";
import SelectInput from "@/components/atoms/forms/SelectInput.js";
import Checkbox from "@/components/atoms/forms/Checkbox.js";
import Radio from "@/components/atoms/forms/Radio";
import SkillInputs1 from "@/components/molecules/SkillInputs1";
import TagInputs from "@/components/molecules/TagInputs";

import instance from "@/utils/instance";
import Submit from "@/components/atoms/forms/Submit";
import { useSelector } from "react-redux";
import SelectBase from "@/components/atoms/forms/SelectBase";

import { da } from "date-fns/locale";

import {
  SetUpPrefecture,
  SetUpCity,
  SetUpStation
} from "@/function/SetupSelectForm";

// スキルのOR検索の最大数
const skill_max_select = 3;
const tag_max_select = 5;

const Page = () => {
  // redux
  const { companyID } = useSelector(state => state.company);

  /* constants */
  // 働く日数: 0-7
  const days = [...Array(8)].map((_, i) => i);

  /* states */
  const [showAddress, setShowAddress] = useState(false);
  const [title, setTitle] = useState("");
  const [startDate, setStartDate] = useState("");
  const [finishDate, setFinishDate] = useState("");
  const [explain, setExplain] = useState("");
  const [postal, setPostal] = useState("");
  const [prefecture, setPrefecture] = useState("");
  const [city, setCity] = useState("");
  const [address, setAddress] = useState("");
  const [station, setStation] = useState("");
  const [dayNum, setDayNum] = useState("");
  const [remote, setRemote] = useState("");
  const [wage, setWage] = useState("");
  const [skills, setSkills] = useState(
    [...Array(skill_max_select)].map(() => ([
      "",//language
      "",//framework
      "0"//level
    ]))
  );
  const [tags, setTags] = useState(
    [...Array(tag_max_select)].map(() => ([
      ""//content_no
    ]))
  );

  /* handlers */
  const handleChange = event => {
    const name = event.target.name;
    const value = event.target.value;

    switch(name){
      case "title":
        setTitle(value);
        break;
      case "explain" :
        setExplain(value);
        break;
      case "start_post" :
        setStartDate(value);
        break;
      case "finish_post" :
        setFinishDate(value);
        break;
      case "address-checkbox" :
        setShowAddress(!showAddress);
      case "postal" :
        setPostal(value);
        break;
      case "prefecture" :
        setPrefecture(value);

        SetUpCity(value);
        SetUpStation(value);
        break;
      case "city" :
        setCity(value);
        break;
      case "address" :
        setAddress(value);
        break;
      case "station" :
        setStation(value);
        break;
      case "dayNum" :
        setDayNum(value);
        break;
      case "remote" :
        setRemote(value);
        break;
      case "wage" :
        setWage(value);
        break;
    }
  };

  const handleSubmit = () => {
    event.preventDefault();

    let data = {
      coId: companyID,
      title: title,
      startDate: formatISO(new Date(startDate)), // 日付をISO8601に変換
      finishDate: formatISO(new Date(finishDate)),
      explain: explain,
      postal: postal,
      prefecture: prefecture,
      city: city,
      address: address,
      dayNum: dayNum,
      wage: wage,
      remote: remote
    };

    const create_recruit_api_url = "http://localhost:4000/api/recruitment/insert";
    const bind_recruit_skill_api_url = "http://localhost:4000/api/skill/insert";
    const bind_recruit_tag_api_url = "http://localhost:4000/api/tag/insert";

    fetch(create_recruit_api_url, {
      method: "POST", // or 'PUT'
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify(data) // data can be `string` or {object}!
    })
      .then(response => {
        if (response.status == 200) {
          return response.json();
        } else alert("プッシュ通知の送信に失敗しました");
      })
      .then(data => {
        alert(`recruit_idは${data.recruit_id}です`); // recruit_idが返ってくる
        console.log(data.recruit_id);

        let skillData = {
          recruit_id: data.recruit_id,
          skills
        }

        let tagData = {
          recruit_id: data.recruit_id,
          tags
        }

        fetch(bind_recruit_skill_api_url, {
          method: "POST", // or 'PUT'
          headers: {
              "Content-type": "application/json"
          },
          body: JSON.stringify(skillData) // data can be `string` or {object}!
        })
          .then(response => {
            if (response.status == 200) {
              return response.json();
            } else alert("プッシュ通知の送信に失敗しました");
          })

        fetch(bind_recruit_tag_api_url, {
          method: "POST", // or 'PUT'
          headers: {
            "Content-type": "application/json"
          },
          body: JSON.stringify(tagData) // data can be `string` or {object}!
        })
          .then(response => {
            if (response.status == 200) {
              return response.json();
            } else alert("プッシュ通知の送信に失敗しました");
          })
      });
  };

  useEffect(() => {
    if (!companyID) {
      console.log("companyID is undefined; returning...");
      return;
    }
    const f = async () => {
      const res = await instance.get(`/company/find/?co_id=${companyID}`); // APIからのレスポンス
      const data = res.data.data; // レスポンスの中の必要なやつだけ
      setPostal(data[0].postal);
      setPrefecture(data[0].prefecture);
      setCity(data[0].city);
      setAddress(data[0].address);
      setStation(data[0].station);

      SetUpPrefecture();
    };
    // 非同期関数呼び出し
    f();
  }, [companyID]);

  return (
    <Layout>
      <main>
        <h1>求人を出す</h1>
        <Card center>
          <div className="card-inner">
            <form id="Form" onSubmit={handleSubmit}>
              <label htmlFor="title">求人タイトル</label>
              <TextInput
                required
                placeholder="求人サービス「TECH-BAITO」のフロントエンド開発"
                type="text"
                name="title"
                value={title}
                width="auto"
                onChange={handleChange}
              />
              <label>求人掲載開始日</label>
              <TextInput
                type="date"
                id="start_post"
                name="start_post"
                value={startDate}
                required
                onChange={handleChange}
              />
              <label>求人掲載終了日</label>
              <TextInput
                type="date"
                id="finish_post"
                name="finish_post"
                value={finishDate}
                required
                onChange={handleChange}
              />
              <label>求人の説明</label>
              <Textarea
                required
                width="auto"
                name="explain"
                value={explain}
                placeholder="React.jsを使ったWebサービスのフロントエンド開発業務をお願いします。"
                onChange={handleChange}
              />
              <label>イメージ画像</label>
              <input type="file" name="picture" />
              <label>勤務地</label>
              <Checkbox
                name="address-checkbox"
                label="会社所在地と異なる住所を使用する"
                onChange={handleChange}
              />
              <div className="address-container">
                <div className="column">
                  <label htmlFor="postal">
                    郵便番号
                    <TextInput
                      pattern="\d{3}-\d{4}"
                      placeholder="000-0000"
                      type="text"
                      name="postal"
                      value={postal}
                      onChange={handleChange}
                    />
                  </label>
                  <div className="select-wrapper">
                    都道府県
                    <SelectBase
                      id="SelectPrefecture"
                      name="prefecture"
                      value={prefecture}
                      onChange={handleChange}
                      width="10rem"
                    />
                  </div>
                  <div className="select-wrapper">
                    市区町村
                    <SelectBase
                      id="SelectCity"
                      name="city"
                      value={city}
                      onChange={handleChange}
                      width="10rem"
                    />
                  </div>
                  <label htmlFor="address">
                  番地・ビル名・号室など
                  <TextInput
                    placeholder="○○番地□□ビル××号室"
                    type="text"
                    name="address"
                    value={address}
                    onChange={handleChange}
                  />
                  </label>
                </div>
                <div className="column">
                  <label>最寄り駅</label>
                  <div className="select-wrapper">
                    <SelectBase
                      id="SelectStation"
                      name="station"
                      value={station}
                      onChange={handleChange}
                      width="10rem"
                    />
                  </div>
                </div>
              </div>
              <div className="form-row">
                <label>最低出勤日数/週</label>
                <SelectInput
                  required
                  name="dayNum"
                  values={days}
                  texts={days.map(item => item + "日")}
                  onChange={handleChange}
                />
              </div>
              <div className="form-row">
                <label>リモート</label>
                <Radio
                  required
                  label="可"
                  name="remote"
                  value="1"
                  onChange={handleChange}
                />
                <Radio
                  required
                  label="不可"
                  name="remote"
                  value="0"
                  onChange={handleChange}
                />
              </div>
              <div className="form-row">
                <label>時給</label>
                <div className="wage-input-wrapper">
                  <TextInput
                    type="text"
                    pattern="\d*"
                    width="5rem"
                    required
                    name="wage"
                    value={wage}
                    onChange={handleChange}
                  />{" "}
                  円
                </div>
              </div>
              <div className="row-container">
                <label>技術</label>
                <div className="skills-container">
                  <div className="skills-row">
                    <SkillInputs1
                      name="skills"
                      setSkills={setSkills}
                      skills={skills}
                      max_select={skill_max_select}
                    />
                  </div>
                </div>
              </div>
              <div className="row-container">
                <label>タグ</label>
                <div className="skills-container">
                  <div className="skills-row">
                    <TagInputs
                      name="tags"
                      setTags={setTags}
                      tags={tags}
                      max_select={tag_max_select}
                    />
                  </div>
                </div>
              </div>
              <div className="submit-wrapper">
                <Submit />
              </div>
            </form>
          </div>
        </Card>
      </main>
      <style jsx>{`
        form {
          display: flex;
          flex-direction: column;
        }
        label {
          margin-bottom: 0.5rem;
          display: flex;
          align-items: center;
          font-weight: 400;
          font-size: 1.1rem;
        }
        label:not(:first-of-type) {
          margin-top: 3rem;
        }
        .form-row {
          display: flex;
          align-items: center;
          margin-top: 3rem;
        }
        .form-row.no-margin {
          margin-top: 0;
        }
        .form-row-tag {
          display: flex;
          margin-top: 3rem;
          align-items: flex-start;
        }
        .form-row > label {
          flex-basis: 12rem;
          margin-bottom: 0;
        }
        .address-container > label {
          display: flex;
          justify-content: space-between;
        }
        main {
          display: flex;
          flex-direction: column;
          align-items: center;
          width: 100%;
        }
        .address-container {
          margin-top: 1rem;
          background: aliceblue;
          padding: 1rem;
          display: ${showAddress ? "block" : "none"};
        }
        .card-inner {
          width: 60vw;
        }

        .input-wrapper {
          display: flex;
        }
        .skills-container {
          display: flex;
          flex-direction: column;
          background: aliceblue;
          padding: 1rem;
        }
        .skills-container:first-child {
          margin-top: 0;
        }
        .select-wrapper {
          margin-right: 1rem;
        }
        .submit-wrapper {
          display: flex;
          justify-content: center;
          margin-top: 2rem;
        }
        .tags-container {
          display: flex;
          flex-flow: row wrap;
          flex-shrink: 1;
        }
        .tag-wrapper {
          margin-bottom: 0.5rem;
          margin-right: 0.5rem;
        }
        .wage-input-wrapper {
          margin: 0.5rem;
        }
      `}</style>
    </Layout>
  );
};
export default Page;
