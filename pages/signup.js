import React, { useState } from "react";

import Layout from "@/components/molecules/Layout";
import Card from "@/components/atoms/Card";
import TextInput from "@/components/atoms/forms/TextInput";
import SelectInput from "@/components/atoms/forms/SelectInput.js";
import Submit from "@/components/atoms/forms/Submit";

import { signUp } from "@/function/firebase-auth-user";

// TODO: ログインの処理実装とか
const Page = () => {
  // 入力を保存するstate
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [occupation, setOccupation] = useState("");
  const [region, setRegion] = useState("");
  const [prefecture, setPrefecture] = useState("");
  const [city, setCity] = useState("");
  const [station, setStation] = useState("");
  // 入力が変更されたときの処理
  const handleChange = event => {
    const name = event.target.name;
    const value = event.target.value;
    if (name === "email") {
      setEmail(value);
      return;
    }
    if (name === "password") {
      setPassword(value);
      return;
    }
    if (name === "occupation") {
      setOccupation(value);
      return;
    }
    if (name === "region") {
      setRegion(value);
      return;
    }
    if (name === "prefecture") {
      setPrefecture(value);
      return;
    }
    if (name === "city") {
      setCity(value);
      return;
    }
    if (name === "station") {
      setStation(value);
      return;
    }
  };

  // form送信の処理
  const handleSubmit = event => {
    // ページのリロードを防ぐ
    event.preventDefault();
    //console.log(email, password, occupation);
    signUp(email, password, function(user, error) {
      //firebaseのユーザ用DBにemailとパスワードでサインアップ
      let userID = user.user.uid;   //uidという名前でuserIDが入ってます
      let data = { user_id: userID, mail: email, occupation: occupation };
      let json = JSON.stringify(data);
      console.log(json);

      const user_signup_api_url = "http://localhost:4000/api/user/insert";

      fetch(user_signup_api_url, {
        method: "POST", // or 'PUT'
        headers: {
          "Content-type": "application/json"
        },
        body: JSON.stringify(data) // data can be `string` or {object}!
      }).then(response => {
        if (response.status == 200) alert("プッシュ通知の送信に成功しました");
        else alert("プッシュ通知の送信に失敗しました");
      });
    });
  };

  return (
    <Layout>
      <main>
        <h1>アカウント作成</h1>
        <Card width="60%" center>
          <section>
            <form onSubmit={handleSubmit}>
              <label htmlFor="email">
                メールアドレス
                <TextInput
                  required
                  placeholder="hoge@mail.com"
                  type="email"
                  name="email"
                  value={email}
                  onChange={handleChange}
                />
              </label>
              <label htmlFor="password">
                パスワード
                <TextInput
                  required
                  name="password"
                  type="password"
                  placeholder="パスワードを入力してください"
                  value={password}
                  onChange={handleChange}
                />
              </label>
              <label htmlFor="occupation">
                職業
                <SelectInput
                  required
                  width="16.2rem"
                  name="occupation"
                  values={["student", "employed", "housewife"]}
                  texts={["学生", "会社員", "主婦"]}
                  value={occupation}
                  onChange={handleChange}
                />
              </label>

              <div className="submit-wrapper">
                <Submit />
              </div>
            </form>
          </section>
        </Card>
      </main>
      <style jsx>{`
        form {
          display: flex;
          flex-direction: column;
        }
        label {
          margin-top: 1rem;
          display: flex;
          justify-content: space-between;
        }
        main {
          display: flex;
          flex-direction: column;
          align-items: center;
          width: 100%;
        }
        .selects-container {
          display: flex;
          width: 16.2rem;
          justify-content: space-between;
        }
        .submit-wrapper {
          display: flex;
          justify-content: center;
          margin-top: 2rem;
        }
      `}</style>
    </Layout>
  );
};

export default Page;
