export default (state = {}, action) => {
  switch (action.type) {
    case "SET_COMPANY": {
      return {
        ...state,
        companyID: action.payload.companyID
      };
    }
    case "UNSET_COMPANY": {
      return {
        ...state,
        companyID: null
      };
    }
    default:
      return state;
  }
};
