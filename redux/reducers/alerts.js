export default (state = {}, action) => {
  switch (action.type) {
    case "SET_ALERTS": {
      return {
        ...state,
        ...action.payload
      };
    }
    default:
      return state;
  }
};
