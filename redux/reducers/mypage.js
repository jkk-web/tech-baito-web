export default (state = {}, action) => {
  switch (action.type) {
    case "SET_MYPAGE": {
      return {
        ...state,
        ...action.payload
      };
    }
    default: {
      return state;
    }
  }
};
