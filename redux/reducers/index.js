import { combineReducers } from "redux";
import alerts from "./alerts";
import company from "./company";
import mypage from "./mypage";
import recruit from "./recruit";
import search from "./search";
import user from "./user";

// combine all Reducers here
const rootReducer = combineReducers({
  alerts,
  company,
  mypage,
  recruit,
  search,
  user
});

export default rootReducer;
