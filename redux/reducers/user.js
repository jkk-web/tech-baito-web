export default (state = {}, action) => {
  switch (action.type) {
    case "SET_USER": {
      return {
        ...state,
        userID: action.payload.userID
      };
    }
    case "UNSET_USER": {
      return {
        ...state,
        userID: null
      };
    }
    default:
      return state;
  }
};
