export const setCompany = companyID => {
  return {
    type: "SET_COMPANY",
    payload: {
      companyID
    }
  };
};

export const unsetCompany = () => {
  return {
    type: "UNSET_COMPANY"
  };
};
