export const setAlerts = data => {
  return {
    type: "SET_ALERTS",
    payload: data
  };
};
