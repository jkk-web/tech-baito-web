export const setMypage = data => {
  return {
    type: "SET_MYPAGE",
    payload: data
  };
};
