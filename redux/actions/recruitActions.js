// 求人検索画面で選択した求人をセット; 中身はデータベースと同じ
export const setRecruit = data => {
  return {
    type: "SET_RECRUIT",
    payload: data
  };
};
