/**
 * @see https://levelup.gitconnected.com/react-redux-hooks-useselector-and-usedispatch-f7d8c7f75cdd
 */

export const setUser = userID => {
  return {
    type: "SET_USER",
    payload: {
      userID
    }
  };
};

export const unsetUser = () => {
  return {
    type: "UNSET_USER"
  };
};
