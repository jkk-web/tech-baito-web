export const setSearch = data => {
  return {
    type: "SET_SEARCH",
    payload: data
  };
};
