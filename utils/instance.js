import axios from "axios";

const instance = axios.create({
  baseURL: 'http://localhost:4000/api/',  //process.env.API_URL,
  timeout: 1000
});

export default instance;
