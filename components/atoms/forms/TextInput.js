import React from "react";

// text, password, email系のinput用
// textのとき type の propsは入れなくてok
// email, passwordのときは type="email"かtype="password"っていれてね
const TextInput = props => {
  const { type, width, ...rest } = props;
  return (
    <>
      {type ? <input type={type} {...rest} /> : <input type="text" {...rest} />}
      <style jsx>{`
        input {
          /* デフォルトのスタイルを削除 */
          -webkit-appearance: none;
          outline: none;
          /* custom styles */
          border-style: dashed;
          border-color: lightblue;
          border-width: 0.1rem;
          padding: 0.5rem;
          width: ${width ? width : "15rem"};
          font-size: 1rem;
        }
        ::placeholder {
          color: lightblue;
        }
      `}</style>
    </>
  );
};

export default TextInput;
