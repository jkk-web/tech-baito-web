import React from "react";

const Textarea = props => {
  const { width, ...rest } = props;

  return (
    <>
      <textarea {...rest}></textarea>
      <style jsx>{`
        textarea {
          /* remove defalult styles */
          -webkit-appearance: none;
          outline: none;
          resize: none;
          /* custom styles */
          border-style: dashed;
          border-color: lightblue;
          border-width: 0.1rem;
          padding: 0.5rem;
          width: ${width ? width : "50%"};
          height: 10rem;
          font-size: 1rem;
        }
        ::placeholder {
          color: lightblue;
        }
      `}</style>
    </>
  );
};

export default Textarea;
