import React from "react";

const Submit = props => {
  const { width, ...rest } = props;
  return (
    <>
      <input type="submit" {...rest} />
      <style jsx>{`
        input {
          /* remove default */
          -webkit-appearance: none;
          outline: none;
          /* custom */
          width: ${width ? width : "fit-content"};
          height: fit-content;
          background: lightblue;
          border: 0.1rem solid lightblue;
          border-radius: 1.5rem;
          box-shadow: 0 0.3rem rgb(155, 194, 207);
          padding: 0.5rem 3rem;
          font-size: 1rem;
          margin-bottom: 0.3rem;
        }
        input:hover {
          cursor: pointer;
          border-color: lightblue;
          box-shadow: 0 0 rgb(155, 194, 207);
          position: relative;
          top: 0.3rem;
        }
        .wrapper {
          display: flex;
          justify-content: center;
        }
      `}</style>
    </>
  );
};

export default Submit;
