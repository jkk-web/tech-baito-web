import React from "react";

// とりあえずラベルも表示する
// props: label, name, value
const Checkbox = props => {
  const { name, value, label, ...rest } = props;
  return (
    <div className="container">
      <input type="checkbox" name={name} value={value} {...rest} />
      <label htmlFor={name}>{label}</label>
      <style jsx>{`
        input {
          /* remove defaults */
          -webkit-appearance: none;
          outline: none;
          /* custom styles */
          border: 2px dashed lightblue;
          padding: 10px; /* rem だと崩れるのでpx; 理由はしらんです... */
          background: white;
          position: relative;
          margin-right: 0.5rem;
        }
        input:checked::after {
          content: "";
          height: 12px;
          width: 12px;
          background: lightblue;
          /* position */
          position: absolute;
          top: 4px;
          left: 4px;
        }
        .container {
          display: flex;
          align-items: center;
          margin-right: 1rem;
        }
      `}</style>
    </div>
  );
};

export default Checkbox;
