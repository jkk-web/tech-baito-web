import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { faStar as farStar } from "@fortawesome/free-regular-svg-icons";

// TODO: globalみたいなやつで設定
const MAX_LEVEL = 5; // レベルの最大値

const Star = props => {
  const { fill, ...rest } = props;
  return (
    <div>
      <FontAwesomeIcon icon={fill ? faStar : farStar} />
      <style jsx>{`
        div {
        }
      `}</style>
    </div>
  );
};

const Skill = props => {
  // category: フロントエンドとかの分野; skillName: ReactとかGitとかスキルの名前
  // note: とりあえずジャンルごとに色分けはしない
  const { category, name, level, ...rest } = props;

  return (
    <div className="container">
      <div>{name}</div>
      <div className="level-container">
        {[...Array(level)].map((_, i) => (
          <Star fill key={i} />
        ))}
        {[...Array(MAX_LEVEL - level)].map((_, i) => (
          <Star key={i} />
        ))}
      </div>
      <style jsx>{`
        .container {
          /* START alt styles */
          /*
          padding: 0.5rem;
          border: 0.1rem solid black;
          */
          /* END alt styles
          /* flex */
          display: flex;
          align-items: center;
          /* size */
          padding: 0.3rem 1rem;
          height: fit-content;
          margin: 0 1rem 1rem 0;
          /* color & shape */
          background: gold;
          border-radius: 2rem;
          /* fonts */
          font-size: 0.8rem;
        }
        .level-container {
          display: flex;
          margin-left: 1rem;
        }
      `}</style>
    </div>
  );
};

export default Skill;
