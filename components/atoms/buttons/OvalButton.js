import React from "react";

const OvalButton = props => {
  const { color, width, children, ...rest } = props;
  return (
    // HTMLのボタンにonClickやらdisabledをそのまま渡せるように ... (スプレッド演算子)を使う
    <button {...rest}>
      {children}
      <style jsx>{`
        button {
          background: ${color ? color : "lightblue"};
          border: 0.1rem solid lightblue;
          border-radius: 1.5rem;
          box-shadow: 0rem 0.3rem rgb(155, 194, 207);
          padding: 0.5rem 3rem;
          font-size: 1rem;
          margin-bottom: 0.3rem;
          width: ${width ? width : "auto"};
          /* flex */
          display: flex;
          justify-content: center;
          align-items: center;
        }
        button:hover {
          cursor: pointer;
          border-color: lightblue;
          box-shadow: 0 0 rgb(155, 194, 207);
          position: relative;
          top: 0.3rem;
        }
        button:focus {
          /* remove default styles */
          outline: none;
        }
        button:disabled {
          cursor: default;
          border-color: lightblue;
          box-shadow: 0 0 rgb(155, 194, 207);
          position: relative;
          top: 0.3rem;
        }
      `}</style>
    </button>
  );
};

export default OvalButton;
