import React from "react";

const Button = props => {
  const { color, shadowColor, width, ...rest } = props;
  const boxShadow = `0 0.3rem ${
    shadowColor ? shadowColor : "rgb(155, 194, 207)"
  }`;
  const boxShadowHover = `0 0 ${
    shadowColor ? shadowColor : "rgb(155, 194, 207)"
  }`;
  return (
    // HTMLのボタンにonClickやらdisabledをそのまま渡せるように ... (スプレッド演算子)を使う
    <button {...rest}>
      {props.children}
      <style jsx>{`
        button {
          /* flex */
          display: flex;
          justify-content: center;
          align-items: center;
          /* other */
          background: ${color ? color : "lightblue"};
          border: ${color ? color : "lightblue"};
          border-radius: 0.3rem;
          padding: 0.5rem;
          font-size: 1rem;
          width: ${width ? width : "auto"};
          /* shadow */
          box-shadow: ${boxShadow};
          margin-bottom: 0.3rem;
        }

        button:hover:not(:disabled) {
          cursor: pointer;
          border-color: lightblue;
          box-shadow: ${boxShadowHover};
          position: relative;
          top: 0.4rem;
        }
        button:focus {
          /* remove default styles */
          outline: none;
        }
      `}</style>
    </button>
  );
};

export default Button;
