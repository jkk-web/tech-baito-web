import React from 'react';
//import { watch } from 'fs';

const PMButton = props => {
  const { color, shadowColor, width, max_select, count_display, what, setCount, info, setInfo, ...rest } = props;
  const boxShadow = `0 0.3rem ${
    shadowColor ? shadowColor : "rgb(155, 194, 207)"
  }`;
  const boxShadowHover = `0 0 ${
    shadowColor ? shadowColor : "rgb(155, 194, 207)"
  }`;


  const buttonHandler = event =>{
    event.preventDefault();
    const name = event.target.name;
    if(what === "skills"){
      switch(name){
        case "add-button":
          setCount(count_display+1);
          break;
        case "decrease-button":
          setInfo(info.map((item, i) =>
            i<count_display
            ?
            [item[0],item[1],item[2],item[3]]
            :["","","all","all"]
            ));
          setCount(count_display-1);
          break;
      }
    }else if(what === "skills1"){
      console.log("ふううう");
      switch(name){
        case "add-button":
          setCount(count_display+1);
          break;
        case "decrease-button":
          setInfo(info.map((item, i) =>
            i<count_display
            ?
            [item[0],item[1],item[2]]
            :["","","all"]
            ));
          setCount(count_display-1);
          break;
      }
    }else{
      switch(name){
        case "add-button":
          setCount(count_display+1);
          break;
        case "decrease-button":
          setInfo(info.map((item, i) =>
            i<count_display
            ?
            item[0]
            :""
            ));
          setCount(count_display-1);
          break;
      }
    }
  };
  return (
    <div className='row-container'>
      <button
      name='add-button'
      onClick={buttonHandler}
      style={{display:count_display==max_select-1?"none":"flex"}}>
        追加
      </button>
      <button
      name='decrease-button'
      onClick={buttonHandler}
      style={{display:count_display==0?"none":"flex"}}>
        削除
      </button>
      <style jsx>{`
        button {
          /* flex */
          /*display: flex;*/
          justify-content: center;
          align-items: center;
          /* other */
          background: ${color ? color : "lightblue"};
          border: ${color ? color : "lightblue"};
          border-radius: 0.3rem;
          padding: 0.5rem;
          font-size: 1rem;
          width: ${width ? width : "auto"};
          /* shadow */
          box-shadow: ${boxShadow};
          margin-bottom: 0.3rem;
        }

        button:hover:not(:disabled) {
          cursor: pointer;
          border-color: lightblue;
          box-shadow: ${boxShadowHover};
          position: relative;
          top: 0.4rem;
        }
        button:focus {
          /* remove default styles */
          outline: none;
        }
        .row-container {
          display: flex;
          margin: 0.5rem 0;
          align-items: center;
        }
      `}</style>
    </div>
  );
};

export default PMButton;


