import React from "react";

const BigButton = props => {
  const { width, ...rest } = props;
  return (
    <button>
      {props.children}
      <style jsx>{`
        button {
          /* remove defaults */
          -webkit-appearance: none;
          border: none;
          border-radius: 1rem;
          background: lightblue;
          font-size: 2rem;
          padding: 2rem;
          width: ${width ? width : "auto"};
        }
        button:focus {
          /* remove default */
          outline: none;
        }
      `}</style>
    </button>
  );
};

export default BigButton;
