import React from "react";

const BoxH2 = props => {
  return (
    <h2>
      {props.children}
      <style jsx>{`
        h2 {
          background: lightblue;
          margin: 0;
          display: flex;
          font-weight: normal;
          font-size: 1rem;
          padding: 0.5rem;
        }
      `}</style>
    </h2>
  );
};

export default BoxH2;
