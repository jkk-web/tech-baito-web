import React from "react";

const Card = props => {
  const { width, color, border, children, ...rest } = props;
  return (
    <div className="container">
      {children}
      <style jsx>{`
        .container {
          background: ${color ? color : "white"};
          border: ${border ? "3px solid lightblue" : "none"};
          border-radius: 1rem;
          padding: 1.5rem;
          /* size */
          width: ${width ? width : "auto"};
          height: fit-content;
        }
      `}</style>
    </div>
  );
};

export default Card;
