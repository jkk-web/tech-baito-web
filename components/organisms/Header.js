import React, { useState, useEffect } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { useSelector, useDispatch } from "react-redux";

import { setUser } from "@/redux/actions/userActions";
import { unsetUser } from "@/redux/actions/userActions";
import { setCompany } from "@/redux/actions/companyActions";
import { unsetCompany } from "@/redux/actions/companyActions";
import { signOut as userSignOut } from "@/function/firebase-auth-user";
import { signOut as companySignOut } from "@/function/firebase-auth-co";
import { confirmSignined as userConfirmSignined } from "@/function/firebase-auth-user";
import { confirmSignined as companyConfirmSignined } from "@/function/firebase-auth-co";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBell,
  faSearch,
  faTerminal,
  faUserAlt,
  faSignInAlt,
  faSignOutAlt,
  faBuilding,
  faPaperPlane,
  faUsers,
  faPencilAlt
} from "@fortawesome/free-solid-svg-icons";

import Alert from "@/components/organisms/Alert";

// 暫定のロゴっぽいやつ
// TODO: ロゴに差し替える
const Square = () => {
  return (
    <>
      <div></div>
      <style jsx>{`
        div {
          background: salmon;
          height: 3rem;
          width: 3rem;
          margin-right: 1rem;
        }
      `}</style>
    </>
  );
};

/* 上向きの三角形 */
const Arrow = props => {
  const { size, ...rest } = props;
  return (
    <div className="arrow">
      <style jsx>{`
        .arrow {
          width: 0;
          height: 0;
          border-left: ${size ? size : "1rem"} solid transparent;
          border-right: ${size ? size : "1rem"} solid transparent;
          border-bottom: ${size ? size : "1rem"} solid white;
        }
      `}</style>
    </div>
  );
};

/* お知らせの中身 (吹き出しのやつ) */
const Alerts = () => {
  // redux
  const alerts = useSelector(state => state.alerts);
  return (
    <div className="container">
      <div className="arrow-wrapper">
        <Arrow size=".7rem" />
      </div>
      <div className="contents">
        {/* 面接不合格・振り分け合格のときは何もしない */}
        {Object.values(alerts).map((item, i) => {
          if (item.state === -1) {
            return (
              <Alert key={i} type="fail">
                {item.co_name}があなたを「不合格」にしました
              </Alert>
            );
          }
          if (item.state === 1) {
            return (
              <Alert key={i} type="sent">
                {item.co_name}があなたを「連絡済み」にしました
              </Alert>
            );
          }
          if (item.state === 2) {
            return (
              <Alert key={i} type="sent">
                {item.co_name}があなたを「採用済み」にしました
              </Alert>
            );
          }
        })}
      </div>
      <style jsx>{`
        .arrow-wrapper {
          align-self: flex-end;
          margin-right: 2rem;
        }
        .container {
          /* flex */
          display: flex;
          flex-direction: column;
          /* position */
          position: absolute;
          top: 5.5rem; /* Headerにあわせてる */
          right: 2rem; /* Headerにあわせてる */
        }
        .contents {
          /* flex */
          display: flex;
          flex-direction: column;
          align-self: flex-end;
          /* size */
          padding: 1rem;
          height: 4rem;
          overflow: scroll;
          /* colors & shape */
          background: white;
          border-radius: 1rem;
        }
      `}</style>
    </div>
  );
};

/* お知らせがあるときだけ表示する丸 */
// TODO: お知らせがあるときだけ表示する
const Badge = () => {
  return (
    <>
      <div />
      <style jsx>{`
        div {
          /* size */
          height: 0.7rem;
          width: 0.7rem;
          /* colors & shape */
          background: salmon;
          border-radius: 50%;
        }
      `}</style>
    </>
  );
};

const Header = () => {
  // redux
  const dispatch = useDispatch();
  const { reduxUserID } = useSelector(state => state.user);
  const { reduxCompanyID } = useSelector(state => state.company);
  // router
  const router = useRouter();
  const isCompany = router.pathname.split("/")[1] === "company"; // 企業ページにいるかどうか
  // state
  const [showAlert, setShowAlert] = useState(false); // お知らせの表示・非表示切り替え
  const [companyID, setCompanyID] = useState("");
  const [userID, setUserID] = useState("");

  useEffect(() => {
    // 非同期関数を定義
    const f = async () => {
      if (!isCompany) {
        if (
          router.pathname != "/signup" &&
          router.pathname != "/login" &&
          router.pathname != "/search"
        ) {
          console.log("ログインチェック");
          userConfirmSignined(function(userId, error) {
            if (userId != "-1") {
              console.log("ログインしてますねえ");
              console.log(userId);
              setUserID(userId);
              dispatch(setUser(userId));
            } else {
              if (router.pathname != "/") {
                alert("ログインしていません");
                location.href = "/";
              }
            }
          });
        }
      } else {
        if (
          router.pathname != "/company/signup" &&
          router.pathname != "/company/login"
        ) {
          companyConfirmSignined(function(coId, error) {
            if (coId != "-1") {
              console.log(coId);
              setCompanyID(coId);
              dispatch(setCompany(coId));
            } else {
              if (router.pathname != "/company") {
                alert("ログインしていません");
                location.href = "/company";
              }
            }
          });
        }
      }
    };
    // 非同期関数呼び出し
    f();
  }, []);

  // ログアウト
  const handleUserLogout = async () => {
    await userSignOut();
    // reduxでuserIDをnullに設定
    dispatch(unsetUser());
    // トップページに遷移
    router.push("/");
  };
  // ログアウト
  const handleCompanyLogout = async () => {
    await companySignOut();
    // reduxでuserIDをnullに設定
    dispatch(unsetCompany());
    // トップページに遷移
    router.push("/");
  };

  const handleAlertClick = () => {
    setShowAlert(!showAlert);
  };

  /* ナビゲーションのリンク・テキスト・アイコン */
  // user, not logged in
  const noUserNav = [
    {
      link: "/search",
      text: "求人を探す",
      icon: faSearch
    },
    {
      link: "/login",
      text: "ログイン/アカウント登録",
      icon: faSignInAlt
    }
  ];
  // user, logged in
  const userNav = [
    {
      link: "/levelcheck",
      text: "課題に挑戦する",
      icon: faTerminal
    },
    {
      link: "/search",
      text: "求人を探す",
      icon: faSearch
    },
    {
      link: "/mypage",
      text: "マイページ",
      icon: faUserAlt
    }
  ];
  // company, not logged in
  // TODO: 資料請求のやつを追加
  const noCompanyNav = [
    {
      link: "/company/login",
      text: "企業ログイン",
      icon: faSignInAlt
    }
  ];
  // company, logged in
  const companyNav = [
    {
      link: "/company/post",
      text: "求人を出す",
      icon: faPencilAlt
    },
    {
      link: "/company",
      text: "応募を見る",
      icon: faUsers
    },
    {
      link: "/company/scout",
      text: "スカウトを送る",
      icon: faPaperPlane
    },
    {
      link: "/company",
      text: "登録情報(TODO)",
      icon: faBuilding
    }
  ];
  // URLとログイン状態に応じてナビゲーションのアイテムを切り替える
  const getNavItems = () => {
    if (isCompany) {
      if (companyID) {
        return companyNav;
      }
      return noCompanyNav;
    }
    if (userID) {
      return userNav;
    }
    return noUserNav;
  };

  return (
    <>
      <div className="container">
        <div className="logo-wrapper">
          <Link href="/">
            <a>
              <Square />
              TECH-BAITO
            </a>
          </Link>
        </div>
        <nav>
          {getNavItems().map((item, i) => (
            <Link key={i} href={item.link}>
              <a>
                <div className="nav-item">
                  <div className="nav-text">{item.text}</div>
                  <div className="nav-icon">
                    <FontAwesomeIcon icon={item.icon} size="lg" />
                  </div>
                </div>
              </a>
            </Link>
          ))}
          {/* ログインしてるときはログアウトのやつを表示する
           */}
          {userID && (
            <div className="nav-item" onClick={handleUserLogout}>
              <div className="nav-text">ログアウト</div>
              <div className="nav-icon">
                <FontAwesomeIcon icon={faSignOutAlt} size="lg" />
              </div>
            </div>
          )}
          {companyID && (
            <div className="nav-item" onClick={handleCompanyLogout}>
              <div className="nav-text">ログアウト</div>
              <div className="nav-icon">
                <FontAwesomeIcon icon={faSignOutAlt} size="lg" />
              </div>
            </div>
          )}
        </nav>
        <div className="alert-container">
          <div className="alert-icon" onClick={handleAlertClick}>
            <FontAwesomeIcon icon={faBell} size="xs" />
          </div>
          <div className="badge-wrapper">
            <Badge />
          </div>
        </div>
      </div>
      {showAlert && <Alerts />}
      <style jsx>{`
        /* START alerts */
        .alert-container {
          /* position */
          position: relative;
          font-size: 2rem;
        }
        .alert-icon:hover {
          cursor: pointer;
        }
        .badge-wrapper {
          position: absolute;
          top: 1rem;
          left: 0.7rem;
        }
        /* END alerts */
        .container {
          padding: 1rem 4rem;
          background: lightblue;
          color: black;
          display: flex;
          align-items: center;
          font-size: 1.3rem;
        }
        .icons-container {
          margin-left: auto;
          display: flex;
          align-items: center;
        }
        .logo-wrapper a {
          display: flex;
          align-items: center;
        }
        /* START navigation */
        a {
          color: black;
        }
        nav {
          font-size: 1rem;
          display: flex;
          justify-content: flex-end;
          flex-grow: 1;
          margin-right: 10vw;
        }

        .nav-item {
          margin-left: 2rem;
          display: flex;
          flex-flow: row wrap-reverse;
          justify-content: center;
        }

        .nav-item:hover {
          cursor: pointer;
        }
        /*
         .nav-item .nav-text {
          display: none;
        }
        .nav-item:hover .nav-text {
          display: block;
        }
        */
        .nav-icon {
          margin-left: 0.5rem;
        }
        /* END navigation */
      `}</style>
    </>
  );
};

export default Header;
