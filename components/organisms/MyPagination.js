import React from "react";
import Pagination from "react-js-pagination";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faAngleLeft,
  faAngleRight,
  faAngleDoubleLeft,
  faAngleDoubleRight
} from "@fortawesome/free-solid-svg-icons";
/**
 *
 * NOTE: スタイルはstyles.cssで定義
 */
const MyPagination = props => {
  const {
    activePage,
    itemsCountPerPage,
    totalItemsCount,
    pageRangeDisplayed,
    onChange,
    ...rest
  } = props;
  return (
    <>
      <Pagination
        activePage={activePage}
        itemsCountPerPage={itemsCountPerPage}
        totalItemsCount={totalItemsCount}
        pageRangeDisplayed={pageRangeDisplayed}
        onChange={onChange}
        innerClass="pg-container"
        itemClass="pg-item"
        linkClass="pg-link"
        prevPageText={<FontAwesomeIcon icon={faAngleLeft} />}
        nextPageText={<FontAwesomeIcon icon={faAngleRight} />}
        firstPageText={<FontAwesomeIcon icon={faAngleDoubleLeft} />}
        lastPageText={<FontAwesomeIcon icon={faAngleDoubleRight} />}
      />
    </>
  );
};

export default MyPagination;
