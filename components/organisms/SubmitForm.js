import React from "react";

import Button from "@/components/atoms/buttons/Button";
import TextInput from "@/components/atoms/forms/TextInput";

/* 課題提出用のフォーム; 動画とGithubURLをつける */
const SubmitForm = () => {
  return (
    <form method="POST">
      <label htmlFor="video">
        <div className="label-text">動画アップロード</div>
        <input type="file" name="video" accept="video/mp4"/>
      </label>
      <label htmlFor="github">
        <div className="label-text">ソースコードのGitHub URL</div>
        <TextInput type="url" name="GitUrl" placeholder="https://github.com/" />
      </label>
      <Button color="salmon" shadowColor="rgb(226, 115, 103)">
        提出
      </Button>
      <style jsx>{`
        form {
          display: flex;
          flex-direction: column;
        }
        label {
          margin-bottom: 1rem;
          display: flex;
          flex-flow: row nowrap;
        }
        .label-text {
          display: flex;
          width: 25vw;
        }
      `}</style>
    </form>
  );
};

export default SubmitForm;
