import React from "react";
import Link from "next/link";
import classNames from "classnames";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHeart,
  faHeartBroken,
  faQuestion
} from "@fortawesome/free-solid-svg-icons";

const Tabs = props => {
  const { current, ...rest } = props; // currentは現在選択されているタブのindex
  const tabs = [
    {
      text: "未審査",
      icon: faQuestion,
      path: "/company/applicants",
      color: "lightblue"
    },
    {
      text: "合格",
      icon: faHeart,
      path: "/company/applicants/pass",
      color: "salmon"
    },
    {
      text: "不合格",
      icon: faHeartBroken,
      path: "/company/applicants/fail",
      color: "plum"
    }
  ];

  return (
    <div className="tab-container">
      {tabs.map((item, i) => {
        const isActive = current === i;
        const tabClass = classNames(
          {
            tab: true,
            "tab-active": isActive
          },
          `tab-${item.color}`
        );
        return (
          <Link key={i} href={item.path}>
            <div className={tabClass}>
              {item.text}
              <div className="icon-wrapper">
                <FontAwesomeIcon icon={item.icon} color="black" />
              </div>
            </div>
          </Link>
        );
      })}
      <style jsx>{`
        .icon-wrapper {
          margin-left: 0.5rem;
        }
        .tab-container {
          display: flex;
          justify-content: space-between;
          align-items: flex-end;
          width: 60vw;
          border-bottom: 2px dashed black;
          padding: 0 1rem;
          margin-bottom: 1rem;
        }
        .tab {
          margin-bottom: -0.5px;
          padding: 0.5rem 0.5rem 0.5rem;
          display: flex;
          border: 2px dashed black;
          border-bottom: none;
        }
        .tab-active {
          padding-bottom: 1.5rem;
          border-style: solid;
          border-bottom: none;
        }
        .tab:hover {
          cursor: pointer;
          padding-bottom: 1.5rem;
        }
        /* tab colors */
        .tab-salmon {
          background: salmon;
        }
        .tab-plum {
          background: plum;
        }
        .tab-lightblue {
          background: lightblue;
        }
      `}</style>
    </div>
  );
};

export default Tabs;
