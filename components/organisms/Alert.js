import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHeartBroken,
  faPaperPlane,
  faFlag
} from "@fortawesome/free-solid-svg-icons";

const Alert = props => {
  // props
  const { type, children, ...rest } = props;

  const getIcon = type => {
    if (type === "fail") {
      return <FontAwesomeIcon icon={faHeartBroken} color="plum" />;
    }
    if (type === "sent")
      return <FontAwesomeIcon icon={faPaperPlane} color="lightblue" />;
    if (type === "hire") return <FontAwesomeIcon icon={faFlag} color="gold" />;
  };
  return (
    <div className="container">
      <div className="icon-wrapper">{getIcon(type)}</div>
      {children}
      <style jsx>{`
        .container {
          display: flex;
          margin-bottom: 1em;
        }
        .close-wrapper {
          margin-left: auto;
        }
        .icon-wrapper {
          margin-right: 0.5rem;
        }
      `}</style>
    </div>
  );
};

export default Alert;
