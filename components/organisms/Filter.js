import React from "react";
import Radio from "@/components/atoms/forms/Radio";

const Filter = () => {
  const handleChange = event => {
    console.log(event.target.value);
  };

  return (
    <div className="container">
      <div className="wrapper">
        <Radio
          label="すべて"
          name="status"
          value="all"
          onChange={handleChange}
        />
      </div>
      <div className="wrapper">
        <Radio
          label="連絡済み"
          name="status"
          value="sent"
          onChange={handleChange}
        />
      </div>
      <div className="wrapper">
        <Radio
          label="未連絡"
          name="status"
          value="unsent"
          onChange={handleChange}
        />
      </div>
      <style jsx>{`
        .container {
          display: flex;
          width: 100%;
          margin-top: 1rem;
        }
      `}</style>
    </div>
  );
};

export default Filter;
