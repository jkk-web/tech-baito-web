import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";

import { setSearch } from "@/redux/actions/searchActions";

import SelectInput from "@/components/atoms/forms/SelectInput";
import Radio from "@/components/atoms/forms/Radio";
import Submit from "@/components/atoms/forms/Submit";
import SkillInputs from "@/components/molecules/SkillInputs";
import TagInputs from "@/components/molecules/TagInputs";


import {
  SetUpPrefecture,
  SetUpCity,
  SetUpStation
} from "@/function/SetupSelectForm";
import SelectBase from "@/components/atoms/forms/SelectBase";

/* constants */
// 時給の値
const wages = [...Array(15)].map((_, i) => 700 + i * 100);
// 働く日数: 0-7
const days = [...Array(8)].map((_, i) => i);
// スキルのOR検索の最大数
const skill_max_select = 3;
const tag_max_select = 5;

const SearchForm = () => {
  /* redux */
  const dispatch = useDispatch();
  /* router */
  const router = useRouter();
  /* state */
  const [prefecture, setPrefecture] = useState("nothing");
  const [city, setCity] = useState("nothing");
  const [station, setStation] = useState("nothing");
  const [maxWage, setMaxWage] = useState(700);
  const [minWage, setMinWage] = useState(700);
  const [maxSift, setMaxSift] = useState(0);
  const [minSift, setMinSift] = useState(0);
  const [remote, setRemote] = useState("either");
  const [skills, setSkills] = useState(
    [...Array(skill_max_select)].map(() => ([
      "",//language
      "",//framework
      "0",//minLevel
      "0"//maxLevel
    ]))
  );
  const [tags, setTags] = useState(
    [...Array(tag_max_select)].map(() => ([
      ""//content_no
    ]))
  );

  /* handlers */
  const handleChange = event => {
    // 処理
    const name = event.target.name;
    const value = event.target.value;
    switch (name) {
      case "prefecture":
        setPrefecture(value);
        SetUpCity(value);
        SetUpStation(value);
        break;
      case "city":
        setCity(value);
        break;
      case "station":
        setStation(value);
        break;
      case "maxWage":
        setMaxWage(parseInt(value, 10));
        break;
      case "minWage":
        setMinWage(parseInt(value, 10));
        break;
      case "maxSift":
        setMaxSift(parseInt(value, 10));
        break;
      case "minSift":
        setMinSift(parseInt(value, 10));
        break;
      case "remote":
        setRemote(value);
        break;
    }
  };

  useEffect(() => {
    // 非同期関数を定義
    const f = async () => {
      try {
        SetUpPrefecture();
      } catch (error) {
        console.log(error);
      }
    };
    // 非同期関数呼び出し
    f();
  }, []);

  // form送信の処理
  const handleSubmit = event => {
    // ページのリロードを防ぐ
    event.preventDefault();

    if (maxWage < minWage) {
      alert("時給の範囲指定に誤りがあります");
      return;
    }
    if (maxSift < minSift) {
      alert("勤務日数の範囲指定に誤りがあります");
      return;
    }

    let data = {
      prefecture,
      city,
      station,
      minWage,
      maxWage,
      minSift,
      maxSift,
      remote,
      skills,
      tags
    };
    console.log(data);

    dispatch(setSearch(data));

    // 検索結果にページ遷移
    router.push("/search/result");
  };

  // スキル
  return (
    <form id="Form" onSubmit={handleSubmit}>
      <div className="row-container">
        <label>勤務地</label>
        <div className="column-container">
          <div className="column">
            <div className="select-wrapper">
              <SelectBase
                id="SelectPrefecture"
                name="prefecture"
                value={prefecture}
                onChange={handleChange}
                width="10rem"
              />
            </div>
            <div className="select-wrapper">
              <SelectBase
                id="SelectCity"
                name="city"
                value={city}
                onChange={handleChange}
                width="10rem"
              />
            </div>
          </div>

          <div className="column">
            <label>最寄り駅</label>
            <div className="select-wrapper">
              <SelectBase
                id="SelectStation"
                name="station"
                value={station}
                onChange={handleChange}
                width="10rem"
              />
            </div>
          </div>
        </div>
      </div>
      <div className="row-container">
        <label>技術</label>
        <div className="skills-container">
          <div className="skills-row">
            <SkillInputs
              name="skills"
              setSkills={setSkills}
              skills={skills}
              max_select={skill_max_select}
            />
          </div>
        </div>
      </div>
      <div className="row-container">
        <label>時給</label>
        <div className="select-wrapper">
          <SelectInput
            name="minWage"
            values={wages}
            texts={wages.map(item => item + "円")}
            onChange={handleChange}
          />
        </div>
        <div className="tilda-wrapper">〜</div>
        <div className="select-wrapper">
          <SelectInput
            name="maxWage"
            values={wages}
            texts={wages.map(item => item + "円")}
            onChange={handleChange}
          />
        </div>
      </div>
      <div className="row-container">
        <label>働く日数</label>
        <div className="text-wrapper">週</div>
        <div className="select-wrapper">
          <SelectInput
            values={days}
            texts={days.map(item => item + "日")}
            name="minSift"
            onChange={handleChange}
          />
        </div>
        <div className="tilda-wrapper">〜</div>
        <div className="text-wrapper">週</div>
        <div className="select-wrapper">
          <SelectInput
            values={days}
            texts={days.map(item => item + "日")}
            name="maxSift"
            onChange={handleChange}
          />
        </div>
      </div>
      <div className="row-container no-border">
        <label>リモート</label>
        <Radio label="あり" name="remote" value="1" onChange={handleChange} />
        <Radio label="なし" name="remote" value="0" onChange={handleChange} />
        <Radio
          label="どちらでも"
          name="remote"
          value="either"
          onChange={handleChange}
        />
      </div>
      <div className="row-container">
        <label>タグ</label>
        <div className="skills-container">
          <div className="skills-row">
            <TagInputs
              name="tags"
              setTags={setTags}
              tags={tags}
              max_select={tag_max_select}
            />
          </div>
        </div>
      </div>
      <div className="submit-wrapper">
        <Submit value="検索" />
      </div>
      <style jsx>{`
        form {
          /* flex */
          display: flex;
          flex-direction: column;
          /* size */
        }
        .checkboxes-container {
          display: flex;
          margin-right: 1rem;
        }
        .column-container {
          display: flex;
          flex-flow: column;
        }
        .column {
          align-items: center;
          display: flex;
        }
        .column > label {
          margin-right: 1rem;
        }
        .column:first-child {
          margin-bottom: 1rem;
        }
        .row-container {
          display: flex;
          margin: 0.5rem 0;
          align-items: center;
          padding-bottom: 1rem;
        }
        .row-container > label:first-child {
          margin-right: 1rem;
          flex-basis: 10rem;
        }
        .select-wrapper {
          margin-right: 1rem;
        }
        .skills-container {
          display: flex;
          flex-direction: column;
          flex-grow: 1;
        }
        .skills-row {
          display: flex;
          align-items: center;
          margin-bottom: 0.5rem;
        }
        .skills-row > label {
          flex-basis: 5rem;
        }
        .submit-wrapper {
          margin-top: 1rem;
          align-self: center;
        }
        .text-wrapper {
          margin-right: 0.5rem;
        }
        .tilda-wrapper {
          margin-right: 1rem;
        }
      `}</style>
    </form>
  );
};

export default SearchForm;
