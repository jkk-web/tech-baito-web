import React, { useState } from "react";
import SkillInput2 from "@/components/molecules/SkillInput2";
import PMButton from "@/components/atoms/buttons/PMButton";

const SkillInputs = props => {
  const { name, setSkills, skills, max_select, ...rest} = props;
  const [count_display, setCount] = useState(0);
  return (
    <div>
      {[...Array(max_select)].map((_, i) => (
        <SkillInput2
          name={name+i.toString()}
          key={name+i.toString()}
          setSkills={setSkills}
          skills={skills}
          number={i}
          count_display={count_display}
          {...rest}
        />
        ))}
        <PMButton
        max_select={max_select}
        count_display={count_display}
        setCount={setCount}
        what="skills"
        info={skills}
        setInfo={setSkills}
        />
    </div>
  )
}
export default SkillInputs;