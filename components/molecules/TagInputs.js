import React, { useState } from "react";
import TagInput from "@/components/molecules/TagInput";
import PMButton from "@/components/atoms/buttons/PMButton";

const TagInputs = props => {
  const { name, setTags, tags, max_select, ...rest} = props;
  const [count_display, setCount] = useState(0);
  return (
    <div>
      {[...Array(max_select)].map((_, i) => (
        <TagInput
          name={name+i.toString()}
          key={name+i.toString()}
          setTags={setTags}
          tags={tags}
          number={i}
          count_display={count_display}
          {...rest}
        />
        ))}
        <PMButton
        max_select={max_select}
        count_display={count_display}
        setCount={setCount}
        what="tags"
        ifno={tags}
        setInfo={setTags}
        />
    </div>
  )
}
export default TagInputs;