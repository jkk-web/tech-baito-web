import React from "react";
import Header from "@/components/organisms/Header";

const Layout = props => {
  return (
    <>
      <div className="container">
        <Header />
        <div className="contents">{props.children}</div>
      </div>
      <style jsx>{`
        .container {
          margin: -8px; /* デフォルトのmargin消す */
          /* flex */
          display: flex;
          flex-direction: column;
          /* size */
          height: 100vh;
        }
        .contents {
          background: papayawhip;
          padding: 2rem;
          /* flex */
          flex-grow: 1;
          display: flex;
          align-items: center;
          flex-direction: column;
        }
      `}</style>
    </>
  );
};

export default Layout;
