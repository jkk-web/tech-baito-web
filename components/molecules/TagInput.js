import React, { useState, useEffect } from "react";
import SelectBase from "@/components/atoms/forms/SelectBase";

import { SetUpTag, RemoveTag } from "@/function/SetupSelectForm";


const TagInput = props => {
  const { name, tags, number, setTags, count_display, ...rest } = props;


  /* function for handler */
  const setTagValue = (value) => {
    setTags(
      tags.map((item, i) =>
        i == number
          ? [value]
          : [item[0]]
      )
    );
  };

  const handleChange = event =>{
    const value = event.target.value;
    setTagValue(value);
    RemoveTag(value,number);
  }

  useEffect(() => {
    // 非同期関数を定義
    const f = async () => {
      try {
        SetUpTag(number);
      } catch (error) {
        console.log(error);
      }
    };
    // 非同期関数呼び出し
    f();
  }, []);


  // スキル
  return (
    <div className="skills-container">
      <div className="skills-row">
        <label htmlFor="tag">タグ</label>
        <SelectBase
          id={name}
          name={name}
          onChange={handleChange}
        />
      </div>
      <style jsx>{`
        .row-container {
          display: flex;
          margin: 0.5rem 0;
          align-items: center;
        }
        .row-container > label:first-child {
          margin-right: 1rem;
          flex-basis: 10rem;
        }
        .select-wrapper {
          margin-right: 1rem;
        }
        .skills-container {
          display: flex;
          flex-direction: column;
          flex-grow: 1;
        }
        .skills-row {
          display: ${number > count_display ? "none" : "flex"};
          align-items: center;
          margin-bottom: 0.5rem;
        }
        .skills-row > label {
          flex-basis: 5rem;
        }
        .text-wrapper {
          margin-right: 0.5rem;
        }
      `}</style>
    </div>
  );
};

export default TagInput;
