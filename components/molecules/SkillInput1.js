import React, { useState, useEffect } from "react";
import SelectBase from "@/components/atoms/forms/SelectBase";
import SelectInput from "@/components/atoms/forms/SelectInput";

import { SetUpLanguage, AddLanguage, RemoveLanguage, SetUpFramework, AddFramework, StackRemoveFramework } from "@/function/SetupSelectForm";

/*レベルの上限*/
const level_number = 5;

const SkillInput1 = props => {
  const { name, skills, number, setSkills, count_display, ...rest } = props;

  const level_params =
    [...Array(level_number+1)].map((_,i)=>
      i != level_number
        ? [i+1, i+1]
          :["all", "指定しない"]
    );

  /* function for handler */
  const setLangValue = (value) => {
    setSkills(
      skills.map((item, i) =>
        i == number
          ? [value, item[1], item[2]]
          : [item[0], item[1], item[2]]
      )
    );
  };

  const setFrameValue = (value) => {
    setSkills(
      skills.map((item, i) =>
        i == number
          ? [item[0], value, item[2]]
          : [item[0], item[1], item[2]]
      )
    );
  };


  const [removedLang, setRemovedLang] = useState("");
  // language用Selectに変更がかかった時の関数
  const handleLang = event => {
    const value = event.target.value;
    console.log(value);
    console.log(removedLang);
    if(removedLang != '')
      AddLanguage(removedLang,number);
    SetUpFramework(value,number);
    RemoveLanguage(value,number);
    setRemovedLang(value);
    setLangValue(value);
  };

  // framework用Selectに変更がかかった時の関数
  const handleFramework = event => {
    const value = event.target.value;
    if(value != '0'){
      StackRemoveFramework(value,number);
      AddLanguage(number);
    }
    setFrameValue(value);
  };

  /*level選択用handler*/
  const handleLevelChange = event => {
    const value = event.target.value;
    setSkills(
      skills.map((item, i) =>
        i == number
          ? [ item[0], item[1], value]
          : [ item[0], item[1], item[2]]
      )
    );
  };

  useEffect(() => {
    // 非同期関数を定義
    const f = async () => {
      try {
        SetUpLanguage(number);
        console.log(number);
        console.log(name);
      } catch (error) {
        console.log(error);
      }
    };
    // 非同期関数呼び出し
    f();
  }, []);


  // スキル
  return (
    <div className="skills-container">
      <div className="skills-row">
        <label htmlFor="programming-language">言語</label>
        <SelectBase
          id={name+"language"}
          name={name+"language"}
          onChange={handleLang}
        />
      </div>
      <div className="skills-row">
        <label htmlFor="framework">フレームワーク</label>
        <SelectBase
          id={name+"framework"}
          onChange={handleFramework}
        />
      </div>
      <div className="skills-row">
        <label htmlFor="level">レベル</label>
        <div className="select-wrapper">
          <SelectInput
            name="min-level"
            values={level_params.map(item=>item[0])}
            texts={level_params.map(item=>item[1])}
            value={skills[number][2]}
            onChange={handleLevelChange}
          />
        </div>
      </div>
      <style jsx>{`
        .row-container {
          display: flex;
          margin: 0.5rem 0;
          align-items: center;
        }
        .row-container > label:first-child {
          margin-right: 1rem;
          flex-basis: 10rem;
        }
        .select-wrapper {
          margin-right: 1rem;
        }
        .skills-container {
          display: flex;
          flex-direction: column;
          flex-grow: 1;
        }
        .skills-row {
          display: ${number > count_display ? "none" : "flex"};
          align-items: center;
          margin-bottom: 0.5rem;
        }
        .skills-row > label {
          flex-basis: 5rem;
        }
        .text-wrapper {
          margin-right: 0.5rem;
        }
      `}</style>
    </div>
  );
};

export default SkillInput1;
