import React, { useState, useEffect } from "react";
import Select from 'react-select';
import SelectInput from "@/components/atoms/forms/SelectInput";

import { SetUpLanguage, SetUpFramework } from "@/function/SetupSelectForm";

/*レベルの上限*/
const level_number = 5;

const SkillInput = props => {
  //最終的にはこっち?
  //const { name_skill_language, name_skill_framework, language_values, language_texts, framework_texts, framework_values, ...rest } = props;
  // update-->SerchFormのstateのskillsを更新するための関数
  //const { name_skill_language, name_skill_framework, update, ...rest } = props;
  const { name, skills, number, setSkills, count_display, ...rest } = props;

  //propsから入るようにする values<-->values, texts<-->labels対応
  let language_values = ["all-lang", "python", "javascript", "C/C++", "html"];
  let language_texts = [
    "指定しない",
    "Python",
    "Javascript",
    "C/C++",
    "HTML"
  ];
  const framework_values = [
    "all-frame",
    "react",
    "vue",
    "nextjs",
    "flask",
    "sass",
    "git"
  ];
  const framework_texts = [
    "指定しない",
    "React",
    "Vue",
    "Nextjs",
    "Flask",
    "Sass",
    "Git"
  ];

  /* state */
  const [skill_language_options, setLangOptions] = useState(
    language_values.map((item, i) => ({
      value: item,
      label: language_texts[i],
      isDisabled: false
    }))
  );
  //sample
  //  let skill_language_options=[{value:"all", label:"all", isDisabled: false},
  //                  {value:"python", label:"python", isDisabled: false},
  //                  {value:"javascript", label:"javascript", isDisabled: false},
  //                  {value:"C/C++", label:"C/C++", isDisabled: false},
  //                  {value:"HTML", label:"HTML", isDisabled: false}
  //                ]

  //const [skill_language_values, setLangValues] = useState([]);
  const [skill_framework_options, setFrameOptions] = useState(
    framework_values.map((item, i) => ({
      value: item,
      label: framework_texts[i],
      isDisabled: false
    }))
  );
  //sample
  //  let skill_framework_options=[{value:"all", label:"all", isDisabled: false},
  //                  {value:"React", label:"React", isDisabled: false},
  //                  {value:"Vue", label:"Vue", isDisabled: true},
  //                  {value:"Nextjs", label:"Nextjs", isDisabled: true},
  //                  {value:"Sass", label:"Sass", isDisabled: true},
  //                  {value:"Git", label:"Git", isDisabled: false}
  //                ]

  //const [skill_framework_values, setFrameValues] = useState([]);

  // idはvalues, optionsと同じ並びにしないといけない
  const language_data_ids = {};
  for (let i in language_values) {
    language_data_ids[language_values[parseInt(i)]] = parseInt(i);
  }
  //const language_data_ids = {"all-lang":0, "python":1, "javascript":2, "C/C++":3, "html":4}

  const framework_data_ids = {};
  for (let i in framework_values) {
    framework_data_ids[framework_values[parseInt(i)]] = parseInt(i);
  }
  //const framework_data_ids = {"all-frame":0, "react":1, "vue":2, "nextjs":3, "flask":4, "sass":5, "git":6}

  //lang-frameの関係, 今は手打ちなのでデータベース化する必要がある
  const data_relation = {
    "all-lang": [...Array(language_data_ids.length).keys()],
    "all-frame": [...Array(framework_data_ids.length).keys()],
    javascript: [1, 2, 3],
    python: [4],
    "C/C++": [],
    html: [],
    react: [2],
    vue: [2],
    nextjs: [2],
    flask: [1],
    sass: [],
    git: []
  };

  const [max_level, setMaxLevel] = useState([]);
  const [min_level, setMinLevel] = useState([]);
  const [min_level_options, setMinLevelOptions] = useState(
    [...Array(level_number + 1)].map((_, i) =>
      i != level_number
        ? { value: i + 1, label: (i + 1).toString(), isDisabled: false }
        : { value: "指定しない", label: "指定しない", isDisabled: false }
    )
  );
  const [max_level_options, setMaxLevelOptions] = useState(
    [...Array(level_number + 1)].map((_, i) =>
      i != level_number
        ? { value: i + 1, label: (i + 1).toString(), isDisabled: false }
        : { value: "指定しない", label: "指定しない", isDisabled: false }
    )
  );

  /* function for handler */
  const setLangValues = values => {
    setSkills(
      skills.map((item, i) =>
        i == number
          ? {
              language: (() =>
                values == [] ? [] : values.map(lists => lists.value))(),
              framework: item.framework,
              maxLevel: item.maxLevel,
              minLevel: item.minLevel
            }
          : {
              language: item.language,
              framework: item.framework,
              maxLevel: item.maxLevel,
              minLevel: item.minLevel
            }
      )
    );
  };

  const setFrameValues = values => {
    setSkills(
      skills.map((item, i) =>
        i == number
          ? {
              language: item.language,
              framework: (() =>
                values == [] ? [] : values.map(lists => lists.value))(),
              maxLevel: item.maxLevel,
              minLevel: item.minLevel
            }
          : {
              language: item.language,
              framework: item.framework,
              maxLevel: item.maxLevel,
              minLevel: item.minLevel
            }
      )
    );
  };

  /* handler */
  // language用Selectに変更がかかった時の関数
  const handleLang = event => {
    // なんかevent.target, event.nameが無い event -> [[value, label, disabled], ...] Selectのoptionsの形で選択されたものが返されてる?
    //const name = event.target.name;

    if (event == null || event.length === 0) {
      // 選択中のものを全部削除したとき
      setLangValues([]);
      setFrameOptions(
        framework_values.map((item, i) => ({
          value: item,
          label: framework_texts[i],
          isDisabled: false
        }))
      );
      setLangOptions(
        language_values.map((item, i) => ({
          value: item,
          label: language_texts[i],
          isDisabled: false
        }))
      );
    } else {
      // 選択追加したとき
      // allを選択したら(選択中のvalueの最後がallなら)
      if (event[event.length - 1].value == "all-lang") {
        //選択中のものをallのみに
        //setLangValues([event[event.length-1]])
        setSkills(
          skills.map((item, i) =>
            i == number
              ? {
                  language: [event[event.length - 1].value],
                  framework: [],
                  maxLevel: item.maxLevel,
                  minLevel: item.minLevel
                }
              : {
                  language: item.language,
                  framework: item.framework,
                  maxLevel: item.maxLevel,
                  minLevel: item.minLevel
                }
          )
        );

        //lang, frame問わず全部選択不可
        setFrameOptions(
          framework_values.map((item, i) => ({
            value: item,
            label: framework_texts[i],
            isDisabled: true
          }))
        );
        setLangOptions(
          language_values.map((item, i) => ({
            value: item,
            label: language_texts[i],
            isDisabled: true
          }))
        );
      } else {
        //setLangValues(event.map((item)=>(item)))
        setLangValues(event);
        // framework用Selectの選択を制限 ex: javascript選択-->reactの選択をできないようにする
        setFrameOptions(
          framework_values.map((item, i) =>
            // Selectのoptionsの生成 [{value:xxx,label:xxx,isDisabled:true or false}, ...]
            ({
              value: item,
              label: framework_texts[i],
              isDisabled: (() => {
                //eventの値(選択中のoptions)から、framework用SelectのisDisabledを設定
                //for(var option of event){
                //  if (data_relation[option["value"]].indexOf(i)!==-1){
                for (var key in event) {
                  if (data_relation[event[key].value].indexOf(i) !== -1) {
                    return true;
                  }
                }
                return false;
              })()
            })
          )
        );
      }
    }
  };

  // framework用Selectに変更がかかった時の関数
  const handleSelect = event => {
    console.log(language_texts);
    if (event == null || event.length === 0) {
      // 選択中のものを全部削除したとき
      setFrameValues([]);
      setLangOptions(
        language_values.map((item, i) => ({
          value: item,
          label: language_texts[i],
          isDisabled: false
        }))
      );
      setFrameOptions(
        framework_values.map((item, i) => ({
          value: item,
          label: framework_texts[i],
          isDisabled: false
        }))
      );
    } else {
      // 選択追加したとき
      // allを選択したら
      if (event[event.length - 1].value == "all-frame") {
        //選択中のものをallのみに
        setSkills(
          skills.map((item, i) =>
            i == number
              ? {
                  language: [],
                  framework: [event[event.length - 1].value],
                  maxLevel: item.maxLevel,
                  minLevel: item.minLevel
                }
              : {
                  language: item.language,
                  framework: item.framework,
                  maxLevel: item.maxLevel,
                  minLevel: item.minLevel
                }
          )
        );

        //lang, frame問わず全部選択不可
        setFrameOptions(
          framework_values.map((item, i) => ({
            value: item,
            label: framework_texts[i],
            isDisabled: true
          }))
        );
        setLangOptions(
          language_values.map((item, i) => ({
            value: item,
            label: language_texts[i],
            isDisabled: true
          }))
        );
      } else {
        setFrameValues(event);
        // language用Selectの選択を制限 ex: javascript選択-->reactの選択をできないようにする
        setLangOptions(
          language_values.map((item, i) =>
            // Selectのoptionsの生成 [{value:xxx,label:xxx,isDisabled:true or false}, ...]
            ({
              value: item,
              label: language_texts[i],
              isDisabled: (() => {
                //eventの値(選択中のoptions)から、language用SelectのisDisabledを設定
                //for(var option of event){
                //  if (data_relation[option["value"]].indexOf(i)!==-1){
                for (var key in event) {
                  if (data_relation[event[key].value].indexOf(i) !== -1) {
                    return true;
                  }
                }
                return false;
              })()
            })
          )
        );
      }
    }
  };

  /*level選択用handler*/
  const minLevelHandler = event => {
    setMaxLevelOptions(
      max_level_options.map((item, i) => ({
        value: item.value,
        label: item.label,
        isDisabled: item.value < event.value ? true : false
      }))
    );
    setMinLevel([
      { value: event.value, label: event.value.toString(), isDisabled: false }
    ]);
    setSkills(
      skills.map((item, i) =>
        i == number
          ? {
              language: item.language,
              framework: item.framework,
              maxLevel: item.maxLevel,
              minLevel: event.value
            }
          : {
              language: item.language,
              framework: item.framework,
              maxLevel: item.maxLevel,
              minLevel: item.minLevel
            }
      )
    );
  };
  const maxLevelHandler = event => {
    setMinLevelOptions(
      min_level_options.map((item, i) => ({
        value: item.value,
        label: item.label,
        isDisabled: item.value > event.value ? true : false
      }))
    );
    setMaxLevel({
      value: event.value,
      label: event.value.toString(),
      isDisabled: false
    });
    setSkills(
      skills.map((item, i) =>
        i == number
          ? {
              language: item.language,
              framework: item.framework,
              maxLevel: event.value,
              minLevel: item.minLevel
            }
          : {
              language: item.language,
              framework: item.framework,
              maxLevel: item.maxLevel,
              minLevel: item.minLevel
            }
      )
    );
  };

  useEffect(() => {
    // 非同期関数を定義
    const f = async () => {
      try {
        //SetUpLanguage(language_values,language_texts);
        //console.log(language_values);
      } catch (error) {
        console.log(error);
      }
    };
    // 非同期関数呼び出し
    f();
  }, []);


  // スキル
  return (
    <div className="skills-container">
      <form id="Form">
      <div className="skills-row">
        <label htmlFor="programming-language">言語</label>
        <Select
          id="language"
          isMulti
          //value={skill_language_values}
          value={
            skills[number].language == undefined ||
            skills[number].language == []
              ? []
              : skills[number]["language"].map(item => ({
                  value: item,
                  label: language_texts[language_values.indexOf(item)],
                  isDisabled:
                    skill_language_options[language_values.indexOf(item)]
                      .isDisabled
                }))
          }
          //name={name+"language"}
          instanceId={name + "language"}
          options={skill_language_options}
          onChange={handleLang}
          {...rest}
        />
      </div>
      <div className="skills-row">
        <label htmlFor="framework">フレームワーク</label>
        <Select
          isMulti
          value={
            skills[number]["framework"] == undefined ||
            skills[number].language == []
              ? []
              : skills[number]["framework"].map(item => ({
                  value: item,
                  label: framework_texts[framework_values.indexOf(item)],
                  isDisabled:
                    skill_framework_options[framework_values.indexOf(item)]
                      .isDisabled
                }))
          }
          //name={name+"framework"}
          instanceId={name + "framework"}
          options={skill_framework_options}
          onChange={handleSelect}
        />
      </div>
      <div className="skills-row">
        <label htmlFor="level">レベル</label>
        <div className="select-wrapper">
          {/*
          <SelectInput
            name="min-level"
            values={[...Array(5)].map((_, i) => i + 1)}
            texts={[...Array(5)].map((_, i) => `${i + 1}`)}
            onChange={levelHandler}
          />
          */}
          <Select
            //name="min-level"
            instanceId={name + "min-level"}
            //value={{value:min_level, label:min_level, isDisabled:false}}
            value={min_level}
            options={min_level_options}
            onChange={minLevelHandler}
          />
        </div>
        <div className="tilda-wrapper">〜</div>
        <div className="select-wrapper">
          {/*
          <SelectInput
            name="max-level"
            values={[...Array(5)].map((_, i) => i + 1)}
            texts={[...Array(5)].map((_, i) => `${i + 1}`)}
            onChange={levelHandler}
          />
          */}
          <Select
            //name="max-level"
            instanceId={name + "max-level"}
            //value={{value:max_level, label:max_level, isDisabled:false}}
            value={max_level}
            options={max_level_options}
            onChange={maxLevelHandler}
          />
        </div>
      </div>
      </form>
      <style jsx>{`
        .row-container {
          display: flex;
          margin: 0.5rem 0;
          align-items: center;
        }
        .row-container > label:first-child {
          margin-right: 1rem;
          flex-basis: 10rem;
        }
        .select-wrapper {
          margin-right: 1rem;
        }
        .skills-container {
          display: flex;
          flex-direction: column;
          flex-grow: 1;
        }
        .skills-row {
          display: ${number > count_display ? "none" : "flex"};
          align-items: center;
          margin-bottom: 0.5rem;
        }
        .skills-row > label {
          flex-basis: 5rem;
        }
        .text-wrapper {
          margin-right: 0.5rem;
        }
      `}</style>
    </div>
  );
};

export default SkillInput;
