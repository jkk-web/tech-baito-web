import React from "react";

import Alert from "@/components/organisms/Alert";

const Alerts = () => {
  return (
    <>
      <h2>お知らせ</h2>
      <div className="alerts-container">
        <Alert type="sent">株式会社Fooがあなたを「連絡済み」にしました</Alert>
        <Alert type="fail">株式会社Hogeがあなたを「不合格」にしました</Alert>
        <Alert type="hire">株式会社Barがあなたを「採用済み」にしました</Alert>
      </div>
      <style jsx>{`
        .alerts-container {
          margin-top: 1rem;
          width: 60vw;
          height: 15vh;
          overflow-y: scroll;
        }
      `}</style>
    </>
  );
};

export default Alerts;
