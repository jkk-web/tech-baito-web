import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHeart,
  faHeartBroken,
  faFlag,
  faQuestion
} from "@fortawesome/free-solid-svg-icons";
import { faFlag as farFlag } from "@fortawesome/free-regular-svg-icons";

const Applications = () => {
  return (
    <>
      <h2>応募した求人</h2>
      <table>
        <thead>
          <tr>
            <th>企業名</th>
            <th>ステータス</th>
            <th>応募した日</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>株式会社A</td>
            <td>
              <div className="td-inner">
                <div className="icon-wrapper">
                  <FontAwesomeIcon icon={faQuestion} />
                </div>
                審査中
              </div>
            </td>
            <td>2020/02/01</td>
          </tr>

          <tr>
            <td>株式会社B</td>
            <td>
              <div className="td-inner">
                <div className="icon-wrapper">
                  <FontAwesomeIcon icon={faHeartBroken} />
                </div>
                不合格
              </div>
            </td>
            <td>2020/02/02</td>
          </tr>
          <tr>
            <td>株式会社D</td>
            <td>
              <div className="td-inner">
                <div className="icon-wrapper">
                  <FontAwesomeIcon icon={faHeart} />
                </div>
                合格
              </div>
            </td>
            <td>2020/01/30</td>
          </tr>
          <tr>
            <td>株式会社C</td>
            <td>
              <div className="td-inner">
                <div className="icon-wrapper">
                  <FontAwesomeIcon icon={faFlag} />
                </div>
                採用
              </div>
            </td>
            <td>2020/01/30</td>
          </tr>
          <tr>
            <td>株式会社E</td>
            <td>
              <div className="td-inner">
                <div className="icon-wrapper">
                  <FontAwesomeIcon icon={farFlag} />
                </div>
                不採用
              </div>
            </td>
            <td>2020/01/30</td>
          </tr>
        </tbody>
      </table>
      <style jsx>{`
        /* START table */
        table {
          width: 60vw;
          margin-top: 1rem;
          border-spacing: 0 1rem;
        }
        th {
          text-align: left;
          border-bottom: 0.1rem dashed rgb(155, 194, 207);
        }
        .td-inner {
          display: flex;
        }
        /* END table */
        .icon-wrapper {
          margin-right: 0.5rem;
        }
      `}</style>
    </>
  );
};

export default Applications;
