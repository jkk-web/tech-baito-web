import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";

import { setMypage } from "@/redux/actions/mypageActions";

import instance from "@/utils/instance";
import Skill from "@/components/atoms/Skill";
import OvalButton from "@/components/atoms/buttons/OvalButton";

const Info = props => {
  // props
  // const { userID, ...rest } = props;
  const { userID } = useSelector(state => state.user);
  // redux
  const dispatch = useDispatch();
  // router
  const router = useRouter();
  // state
  const [UserData, setUserData] = useState([]); // APIのデータ

  useEffect(() => {
    // 非同期関数を定義
    if (!userID) {
      console.log("userID is undefined; returning...");
      return;
    }
    const f = async () => {
      try {
        console.log("ユーザ情報持ってくるよ");
        console.log(userID);
        const res = await instance.get(`/user/find/?user_id=${userID}`); // APIからのレスポンス
        const data = res.data.data; // レスポンスの中の必要なやつだけ
        console.log(data);
        setUserData(data);
      } catch (error) {
        console.log(error);
      }
    };
    // 非同期関数呼び出し
    f();
  }, [userID]);

  // 編集ボタンをクリックしたときの処理
  const handleClick = () => {
    // redux: storeにdispatch
    console.log(UserData);
    alert(UserData);
    dispatch(setMypage(UserData));
    // 編集ページに遷移
    router.push("/mypage/edit");
  };
  return (
    <>
      <h2>基本情報</h2>
      {UserData.map((item, i) => (
        <dl key={i}>
          <dt>名前</dt>
          <dd>{item.name ? item.name : "未設定"}</dd>
          <dt>メールアドレス</dt>
          <dd>{item.mail ? item.mail : "未設定"}</dd>
          <dt>身分</dt>
          <dd>
            {item.occupation === "employed"
              ? "社会人"
              : item.occupation === "student"
              ? "学生"
              : "主婦"}
          </dd>
          <dt>ポートフォリオ</dt>
          <dd>{item.portfolio ? item.portfolio : "未設定"}</dd>
          <dt>自己PR</dt>
          <dd>{item.pr ? item.pr : "未設定"}</dd>
          <dt>到達レベル</dt>
          <dd>
            <Skill name="Github" category="common" level={2} />
            <Skill name="React.js" level={3} category="frontend" />
            <Skill name="SASS" level={2} category="frontend" />
            <Skill name="npm/yarn" level={1} category="frontend" />
            <Skill name="PostgreSQL" level={1} category="backend" />
            <Skill name="Nginx" level={1} category="backend" />
          </dd>
        </dl>
      ))}
      <div className="button-wrapper">
        <OvalButton onClick={handleClick}>編集</OvalButton>
      </div>
      <style jsx>{`
        dl {
          display: flex;
          flex-flow: row wrap;
          width: 60vw;
        }
        dt {
          flex-basis: 20%;
          margin-bottom: 1rem;
        }
        dd {
          margin-bottom: 2rem;
          /* flex */
          display: flex;
          flex-flow: row wrap;
          flex-basis: 70%;
        }
        .button-wrapper {
          display: flex;
          justify-content: center;
        }
      `}</style>
    </>
  );
};

export default Info;
